import { useNavigate, Link } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import '../styles/listProducts.css'
import { Auth, API } from 'aws-amplify'
import * as queries from '../graphql/queries'
import Header from './header'
import { connect } from 'react-redux'

function ListUsers(props) {
  const navigate = useNavigate()
  const [allUsers, setAllUsers] = useState([])
  const [term, setTerm] = useState('')

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then(async (user) => {
        await getItemsByType()
      })
      .catch((err) => {
        console.log('err ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.owner.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemsByType = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemsByType,
        variables: { type: 'AllMeshCRMUser', status: 'live' },
      })
      setAllUsers(allTodos.data.getItemsByType)
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  return (
    <div style={{ padding: '10px' }}>
      <Header />
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <p className="header">{allUsers.length} User(s)</p>
          <input
            className="searchInputBox"
            value={term}
            onChange={(e) => setTerm(e.target.value)}
            placeholder="search by name"
          />
        </div>
        <div className="productRow productRowHeading">
          <p style={{ width: 150, fontSize: 18 }}>User Name</p>
          <p style={{ width: 150, fontSize: 18 }}>Role</p>
          <p style={{ width: 40, fontSize: 18 }}>Edit</p>
        </div>
        {(term == ''
          ? allUsers
          : allUsers.filter(
              (item) => item.username.toString().toLowerCase().indexOf(term.toLowerCase()) > -1,
            )
        ).map((item) => {
          return (
            <>
              <div className="productRow">
                <p style={{ width: 150 }}>{item.username}</p>
                <p style={{ width: 150 }}> {item.role}</p>
                <span style={{ width: 40 }}>
                  <button className="editButtonContainer">
                    <Link to={`../AddUsers/${item.id}`} className="editButton">
                      Edit
                    </Link>
                  </button>
                </span>
              </div>
            </>
          )
        })}
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}
function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(ListUsers)
