import { useNavigate, Link } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import '../styles/listProducts.css'
import { Auth, API } from 'aws-amplify'
import * as queries from '../graphql/queries'
import * as mutations from '../graphql/mutations'
import { RiDeleteBin6Line } from 'react-icons/ri'
import Header from './header'
import { connect } from 'react-redux'
import allmesh from '../images/allmesh.png'

function ListComponents(props) {
  const navigate = useNavigate()
  const [allProducts, SetAllProducts] = useState([])
  const [term, setTerm] = useState('')

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then(async (user) => {
        await getItemsByType()
      })
      .catch((err) => {
        console.log('err ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.admin.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemsByType = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemsByType,
        variables: { type: 'Component', status: 'live' },
      })
      // console.log(allTodos.data.getItemsByType)
      SetAllProducts(allTodos.data.getItemsByType)
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const deleteItem = async (id) => {
    try {
      await API.graphql({
        query: mutations.deleteItem,
        variables: { id: id },
      })
      alert('Component Deleted')
      getItemsByType()
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  return (
    <div style={{ padding: '10px' }}>
      <Header />
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <p className="header">{allProducts.length} component(s) added by you</p>
          <input
            className="searchInputBox"
            value={term}
            onChange={(e) => setTerm(e.target.value)}
            placeholder="search by name"
          />
          <button className="addNewProdButtonContainer">
            <Link to="../AddComponents/addNewComponent" className="addNewProdButton">
              Add New Component
            </Link>
          </button>
        </div>
        <div className="productRow productRowHeading">
          <p style={{ width: 100, fontSize: 18 }}>Images</p>
          <p style={{ width: 150, fontSize: 18 }}>Name</p>
          <p style={{ width: 150, fontSize: 18 }}>Description</p>
          <p style={{ width: 70, fontSize: 18 }}>UOM</p>
          <p style={{ width: 70, fontSize: 18 }}>Price</p>

          <p style={{ width: 40, fontSize: 18 }}>Edit</p>
          <p style={{ width: 40, fontSize: 18 }}>Delete</p>
        </div>
        {(term == ''
          ? allProducts
          : allProducts.filter(
              (item) => item.productName.toString().toLowerCase().indexOf(term.toLowerCase()) > -1,
            )
        ).map((item) => {
          return (
            <>
              <div className="productRow">
                <div style={{ paddingBottom: '5px', paddingTop: '5px', width: 100 }}>
                  <img
                    src={item.productImages != '' ? item.productImages : allmesh}
                    height={80}
                    width={100}
                  />
                </div>

                <p style={{ width: 150 }}>{item.productName}</p>
                <p style={{ width: 150 }}> {item.productDescription}</p>
                <p style={{ width: 70 }}> {item.unitOfMeasurement}</p>
                <a style={{ width: 70 }}>{parseFloat(item.productPrice).toFixed(2)}</a>

                <span style={{ width: 40 }}>
                  <button className="editButtonContainer">
                    <Link to={`../AddComponents/${item.id}`} className="editButton">
                      Edit
                    </Link>
                  </button>
                </span>
                <span style={{ width: 40 }}>
                  <button onClick={() => deleteItem(item.id)} className="deleteIcon">
                    <RiDeleteBin6Line />
                  </button>
                </span>
              </div>
            </>
          )
        })}
      </div>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}

function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(ListComponents)
