import { useNavigate, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import '../styles/addBanner.css'
import { Auth, API } from 'aws-amplify'
import * as mutations from '../graphql/mutations'
import * as queries from '../graphql/queries'
import Header from './header'
import { useSelector, useDispatch, connect } from 'react-redux'
// const uuidv4 = require('uuid/v4')
import {v4 as uuidv4} from 'uuid';

function AddEmail(props) {
  const navigate = useNavigate()
  const [email, setEmail] = useState('')
  const [name, setName] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [group, setGroup] = useState('')
  const [role, setRole] = useState('')
  const [notes, setNotes] = useState('')
  const [addedBy, setAddedBy] = useState('')
  const [spinnerState, setSpinnerState] = useState(false)
  const groupArray = useSelector((state) => {
    return state.common.groupArray
  })
  const roleArray = useSelector((state) => {
    return state.common.roleArray
  })

  let test = useParams()

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then(async (user) => {
        setAddedBy(user.username)
        if (test.test != 'addEMail') await getItemById()
      })
      .catch((err) => {
        console.log('currentAuthenticatedUser ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.manager.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemById = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemById,
        variables: { id: test.test },
      })
      // console.log(allTodos.data.getItemById);
      let res = allTodos.data.getItemById
      if (res == null) navigate('/listEMail')
      else {
        setEmail(res.email)
        setName(res.name)
        setPhoneNumber(res.phoneNumber.slice(3, 13))
        setGroup(res.group)
        setRole(res.role)
        setNotes(res.notes)
      }
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const manageEmail = async () => {
    try {
      await API.graphql({
        query: mutations.manageEmail,
        variables: {
          email: email,
          addedBy: addedBy,
          type: 'Email',
          action: 'addEmail',
          name: name,
          phoneNumber: '+91' + phoneNumber,
          group: group,
          role: role,
          notes: notes,
        },
      })
      alert('Email Added')
      navigate('/listEMail')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const editItem = async () => {
    try {
      await API.graphql({
        query: mutations.editItem,
        variables: {
          id: test.test,
          name: name,
          phoneNumber: '+91' + phoneNumber,
          group: group,
          role: role,
          notes: notes,
        },
      })
      alert('Email details updated')
      navigate('/listEMail')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const checkInput = async () => {
    var mailReg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    if (email != '' && mailReg.test(email) == false) return alert('Please enter valid email')
    if (name == '') return alert('Please enter name')
    var phoneReg = /^[5-9]{1}[0-9]{9}$/
    if (phoneNumber !== '' && phoneReg.test(phoneNumber) === false)
      return alert('Please enter valid phone number')
    if (group == '') return alert('Please select group')
    if (role == '') return alert('Please select role')

    if (test.test === 'addEMail') manageEmail()
    else editItem()
  }

  return (
    <div>
      <Header />
      <div>
        <div>
          <p className="addBannerTitle">Add E-mail Id</p>
        </div>
        <div style={{ width: '400px', marginLeft: '20px' }}>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">E-mail id: </span>
            {(test.test === 'addEMail' && (
              <input
                value={email}
                className="addBannerInputStyle"
                onChange={(e) => setEmail(e.target.value)}
                maxLength={100}
              />
            )) || <span>{email}</span>}
          </div>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Name: </span>
            <input
              value={name}
              className="addBannerInputStyle"
              onChange={(e) => setName(e.target.value)}
              maxLength={100}
            />
          </div>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Phone Number:</span>
            <div style={{ direction: 'flex' }}>
              <span className="addProdInputStyle">+91</span>
              <input
                value={phoneNumber}
                className="addBannerInputStyle"
                onChange={(e) => setPhoneNumber(e.target.value)}
                maxLength={10}
              />
            </div>
          </div>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Group: </span>
            <select
              value={group}
              onChange={(e) => {
                setGroup(e.target.value)
              }}
            >
              <option value="">Select Group</option>;
              {groupArray.map((item) => {
                return <option value={item}>{item}</option>
              })}
            </select>
          </div>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Role: </span>
            <select
              value={role}
              onChange={(e) => {
                setRole(e.target.value)
              }}
            >
              <option value="">Select Role</option>;
              {roleArray.map((item) => {
                return <option value={item}>{item}</option>
              })}
            </select>
          </div>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Notes: </span>
            <input
              value={notes}
              className="addBannerInputStyle"
              onChange={(e) => setNotes(e.target.value)}
              maxLength={200}
            />
          </div>

          <p />
          <div className="addBannerFieldStyle">
            <button disabled={spinnerState} onClick={() => checkInput()} className="addProductBtn">
              {test.test != 'addEMail' ? 'Edit' : 'Add'} e-mail ID
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}
function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(AddEmail)
