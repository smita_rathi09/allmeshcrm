import { useNavigate, Link } from 'react-router-dom'
import { useState } from 'react'
import '../styles/signIn.css'
import react from 'react'
import { Auth } from 'aws-amplify'

export default function SignUp() {
  // let history = useHistory();
  const navigate = useNavigate()
  const [username, setUserName] = useState('')
  const [password, setPassword] = useState('')
  const [preferred_username, setPreferredUserName] = useState('preferred_username')
  const [name, setName] = useState('NA')
  const [spinnerState, setSpinnerState] = useState(false)

  const signUpUser = async () => {
    try {
      const { user } = await Auth.signUp({
        username,
        password,
        attributes: {
          preferred_username, // optional
        },
      })
      navigate('/')
    } catch (error) {
      if (error.code === 'UsernameExistsException')
        alert('You already have an account, Please SignIn')
      else console.log('error signing up:', error)
    }
  }

  const checkInput = () => {
    if (username === '') return alert('Please enter user name')
    if (password === '') return alert('Please enter password')

    signUpUser()
  }

  return (
    <div style={{ background: '#f3f3f3' }}>
      <p className="welcomeTitle">Welcome to AllMesh CRM Portal</p>
      <div className="mainContainer">
        <div className="signInDiv">
          <p className="title1">Enter your details to login</p>
          <div className="credentialsDiv">
            <div className="phnNumIconDiv">
              <p className="userNamePassWord">User Name</p>
            </div>
            <div className="phnNumDiv">
              <div>
                <span className="phnNumCont">
                  <input value={username} onChange={(e) => setUserName(e.target.value)} />
                </span>
              </div>
            </div>
            <div className="phnNumIconDiv">
              <p className="userNamePassWord">Password</p>
            </div>
            <div className="phnNumDiv">
              <div>
                <span className="phnNumCont">
                  <input
                    value={password}
                    type="password"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </span>
              </div>
            </div>
            <div className="buttonDiv">
              <button disabled={spinnerState} className="button" onClick={() => checkInput()}>
                Sign Up
              </button>
              <p>
                Already have an account?{' '}
                <span>
                  <Link to="/" className="signUpBtn">
                    Sign-In
                  </Link>
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
