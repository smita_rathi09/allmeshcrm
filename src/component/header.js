import React, { useEffect } from 'react'
import '../styles/headers.css'
import { useNavigate, Link } from 'react-router-dom'
import { Auth, API } from 'aws-amplify'
import { connect } from 'react-redux'
import allmesh from '../images/allmesh1.png'
import { FaLongArrowAltLeft } from 'react-icons/fa'
function HomePage(props) {
  const navigate = useNavigate()

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        // console.log("user " + JSON.stringify(user));
      })
      .catch((err) => {
        console.log('err ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
  }, [])

  const signOut = async () => {
    Auth.signOut()
    navigate('/')
  }
  const goHome = async () => {
    navigate('/homePage')
  }
  const goBack = async () => {
    navigate(-1)
  }

  return (
    <div>
      <div className="homeLinksContainer">
        {(props.id != 'homepage' && (
          <span className="backlink" onClick={() => goBack()}>
            <FaLongArrowAltLeft />
          </span>
        )) || <span />}

        <div>
          <img src={allmesh} height={35} width={120} />
        </div>

        <div>
          <span className="homeLink" onClick={() => goHome()}>
            Home
          </span>
          <span
            className="logoutLink"
            onClick={() => {
              props.userLogout()
              signOut()
            }}
          >
            Log-out
          </span>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    userLogout: (data) => dispatch({ type: 'USER_LOGOUT', payload: data }),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
