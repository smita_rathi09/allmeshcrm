import { useNavigate, Link } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import { API, Auth } from 'aws-amplify'
import '../styles/listQuotations.css'
import { DataGrid } from '@mui/x-data-grid'
import * as queries from '../graphql/queries'
import Modal from 'react-modal'
import { AiOutlineCloseCircle } from 'react-icons/ai'
import Header from './header'
import moment from 'moment'
import { connect } from 'react-redux'

function ListQuotation(props) {
  const navigate = useNavigate()
  const [allQuotation, setAllQuotation] = useState([])
  const [queryDetails, setQueryDetails] = useState([])
  const [status, setStatus] = useState('')

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        getQuotations()
      })
      .catch((err) => {
        console.log('err ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.sales.includes(props.common.role)) navigate('/homePage')
  }, [])

  const [modal, setModal] = useState(false)

  function orderDetails(data) {
    // console.log(JSON.parse(data.row.orderDetails))
    setQueryDetails(JSON.parse(data.row.orderDetails))
    setModal(true)
  }
  const customStyles = {
    content: {
      top: '50%',
      width: '800px',
      height: '400px',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  }

  const getQuotations = async (status) => {
    try {
      const allTodos = await API.graphql({
        query: queries.getAllItemsByType,
        variables: { type: 'Quotation' },
      })
      setAllQuotation(
        allTodos.data.getAllItemsByType
          .filter((item) => item.status !== 'deleted')
          .sort(
            (a, b) =>
              moment(b.addedDate + ' ' + b.time, 'YYYY-MM-DD  HH:mm:ss') -
              moment(a.addedDate + ' ' + a.time, 'YYYY-MM-DD  HH:mm:ss'),
          ),
      )
    } catch (error) {
      console.log('error', JSON.stringify(error))
    }
  }

  return (
    <div>
      <Header />
      <div>
        <div>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            <p className="header">
              You have{' '}
              {status == ''
                ? allQuotation.length
                : allQuotation.filter((item) => item.status === status).length}{' '}
              quotations
            </p>
            <div>
              <span>Select status: </span>
              <select
                value={status}
                onChange={(e) => {
                  setStatus(e.target.value)
                  getQuotations(e.target.value)
                }}
              >
                <option value="">Select</option>;<option value="live">New</option>;
                <option value="visitScheduled">Visit Scheduled</option>;
                <option value="convertedToOrder">Converted to Order</option>;
                <option value="rejected">Rejected</option>;<option value="expired">Expired</option>;
              </select>
            </div>
            <button className="addNewProdButtonContainer">
              <Link to="../ViewQuotation/addNewQuotation/quotation" className="addNewProdButton">
                Add New Quotation
              </Link>
            </button>
          </div>

          <p></p>

          <div style={{ height: 550, width: '100%' }}>
            <DataGrid
              hover
              // rows={allQuotation.filter((item) => item.status === status)}
              rows={
                status == '' ? allQuotation : allQuotation.filter((item) => item.status === status)
              }
              columns={[
                {
                  field: 'customerName',
                  headerName: 'Customer Name',
                  width: 250,
                },
                {
                  field: 'customerPhone',
                  headerName: 'Phone Number',
                  width: 130,
                },
                { field: 'customerEmail', headerName: 'Email', width: 220 },
                {
                  field: 'estimatedAmount',
                  valueFormatter: (params) => {
                    return '₹ ' + parseFloat(params.value).toFixed(2)
                  },
                  headerName: 'Amount (₹)',
                  width: 120,
                },
                {
                  field: 'addedDate',
                  valueFormatter: (params) => {
                    return moment(params.value, 'YYYY-MM-DD').format('D MMM')
                  },
                  headerName: 'Enquiry Date',
                  width: 120,
                },
                {
                  field: 'time',
                  valueFormatter: (params) => {
                    return moment(params.value, 'HH:mm:ss').format('hh:mm A')
                  },
                  headerName: 'Enquiry Time',
                  width: 120,
                },
                {
                  field: 'status',
                  valueFormatter: (params) => {
                    return params.value == 'live'
                      ? 'Live'
                      : params.value == 'visitScheduled'
                      ? 'Visit Scheduled'
                      : params.value == 'convertedToOrder'
                      ? 'Converted To Order'
                      : params.value == 'rejected'
                      ? 'Rejected'
                      : params.value == 'expired'
                      ? 'Expired'
                      : 'Others'
                  },
                  headerName: 'Status',
                  width: 160,
                },
                {
                  field: 'orderDetails',
                  headerName: 'Query Details',
                  width: 130,
                  renderCell: (data) => {
                    return (
                      <>
                        <button className="viewDetails" onClick={() => orderDetails(data)}>
                          View Details
                        </button>
                      </>
                    )
                  },
                },
                {
                  field: 'id',
                  headerName: 'Quotation',
                  width: 90,
                  renderCell: (id) => {
                    return (
                      <>
                        <Link to={`../ViewQuotation/${id.row.id}/quotation`} className="editButton">
                          {id.row.status == 'expired' ? 'Re-New' : 'View'}
                        </Link>
                      </>
                    )
                  },
                },
              ]}
              pageSize={8}
              rowsPerPageOptions={[8]}
            />
            <Modal isOpen={modal} style={customStyles}>
              <div>
                <div className="closeButtonContainer">
                  <button className="closeButton" onClick={() => setModal(false)}>
                    <AiOutlineCloseCircle />
                  </button>
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <p style={{ fontWeight: 'bold', width: 130 }}>Product Name</p>
                  <p style={{ fontWeight: 'bold', width: 100 }}>Width (mm)</p>
                  <p style={{ fontWeight: 'bold', width: 100 }}>Height (mm)</p>
                  <p style={{ fontWeight: 'bold', width: 100 }}>Panel Count</p>
                  <p style={{ fontWeight: 'bold', width: 100 }}>Quantity</p>
                  <p style={{ fontWeight: 'bold', width: 100 }}>Color</p>
                  <p style={{ fontWeight: 'bold', width: 100 }}>Price (₹)</p>
                  <p style={{ fontWeight: 'bold', width: 100 }}>Total (₹) </p>
                </div>
                {Object.entries(queryDetails).map(([key, data]) => {
                  return (
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                      }}
                    >
                      <p style={{ width: 130 }}>
                        {data.productName}({data.productDescription})
                      </p>
                      <p style={{ width: 100 }}>{data.width}</p>
                      <p style={{ width: 100 }}>{data.height}</p>
                      <p style={{ width: 100 }}>{data.panelCount}</p>
                      <p style={{ width: 100 }}>{data.quantity}</p>
                      <p style={{ width: 100 }}>{data.color}</p>
                      <p style={{ width: 100 }}>{parseFloat(data.itemPrice).toFixed(2)}</p>
                      <p style={{ width: 100 }}>₹ {parseFloat(data.itemTotal).toFixed(2)}</p>
                    </div>
                  )
                })}
              </div>
            </Modal>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
    bill: state.bill,
  }
}

function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(ListQuotation)
