import { actionIcon } from 'aws-amplify'
import {
  ADD_ITEM,
  REMOVE_ITEM,
  SET_BILL_ITEMS,
  SET_COMPONENTS,
  ADD_COMPONENT,
  REMOVE_COMPONENT,
  SET_EXTRA_CHARGES,
  CHANGE_COM_COUNT,
  SET_AEAR_SQFT,
  SET_COMPONENTS_EDIT,
  INCREMENT_ITEM,
  DECREMENT_ITEM,
  COLOR_CHANGED,
  COLOR_CHARGES_CHANGED,
} from '../action'
// const uuidv4 = require('uuid/v4')
import {v4 as uuidv4} from 'uuid';

const initialState = {
  total: 0,
  subTotal: 0,
  items: {},
  groupTotal: 0,
  componentList: {},
  GST: 0,
  GSTAmount: 0,
  discountValue: 0,
  discountType: '',
  discountAmount: 0,
  groupSubTotal: 0,
  groupPrice: 0,
  productMargin: 0,
  marginAmount: 0,
  areaInSqFt: 0,
  colorCharges: 0,
}

export default function cartReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_ITEM:
      //add
      var tempObj = state.items
      var amount = state.total
      var subTotal = state.subTotal
      var id = ''
      var discount = 0
      var GSTAmount = 0
      var colorCharges = 0

      if (Object.keys(state.items).includes(action.payload.key)) {
        id = action.payload.key
        subTotal =
          subTotal + parseFloat(action.payload.total) - parseFloat(action.payload.item.itemTotal)
      } else {
        id = uuidv4()
        tempObj[id] = {}
        tempObj[id].id = action.payload.item.id
        tempObj[id].productName = action.payload.item.productName

        subTotal = subTotal + parseFloat(action.payload.total)
      }
      tempObj[id].productDescription = action.payload.description
      tempObj[id].panelCount = action.payload.panelCount
      tempObj[id].height = action.payload.height
      tempObj[id].width = action.payload.width
      tempObj[id].color = action.payload.color
      tempObj[id].areaInSqFt = action.payload.areaInSqFt
      tempObj[id].componentList = action.payload.componentList
      tempObj[id].productLocation = action.payload.productLocation
      tempObj[id].productType = action.payload.productType
      tempObj[id].itemSubTotal = action.payload.subTotal
      tempObj[id].marginAmount = action.payload.marginAmount
      tempObj[id].productMargin = action.payload.productMargin
      tempObj[id].quantity = action.payload.quantity
      tempObj[id].itemPrice = action.payload.price
      tempObj[id].itemTotal = action.payload.total
      tempObj[id].selectedColorCharge = action.payload.selectedColorCharge
      tempObj[id].colorCharges = action.payload.colorCharges

      if (state.discountValue != '')
        if (state.discountType === 'flat') discount = state.discountValue
        else discount = (parseFloat(state.discountValue) / 100) * parseFloat(subTotal)

      amount = parseFloat(subTotal) - parseFloat(discount)

      if (state.GST != '') GSTAmount = (parseFloat(state.GST) / 100) * parseFloat(amount)
      amount = parseFloat(amount) + parseFloat(GSTAmount)

      return {
        ...state,
        items: tempObj,
        total: amount,
        subTotal: subTotal,
        discountAmount: discount,
        GSTAmount: GSTAmount,
        colorCharges: colorCharges,
      }

    // break;
    case REMOVE_ITEM:
      //remove
      var tempObj = state.items
      var amount = state.total
      var subTotal = state.subTotal
      var discount = 0
      var GSTAmount = 0

      subTotal -= state.items[action.payload].itemTotal
      delete tempObj[action.payload]

      if (state.discountValue != '')
        if (state.discountType === 'flat') discount = state.discountValue
        else discount = (parseFloat(state.discountValue) / 100) * parseFloat(subTotal)

      amount = parseFloat(subTotal) - parseFloat(discount)
      if (state.GST != '') GSTAmount = (parseFloat(state.GST) / 100) * parseFloat(amount)
      amount = parseFloat(amount) + parseFloat(GSTAmount)

      return {
        ...state,
        items: tempObj,
        total: amount,
        subTotal: subTotal,
        discountAmount: discount,
        GSTAmount: GSTAmount,
      }

    case INCREMENT_ITEM:
      //increment
      var tempObj = state.items
      var amount = state.total
      var subTotal = state.subTotal
      var discount = 0
      var GSTAmount = 0
      var colorCharges = 0
      var key = action.payload.key

      tempObj[key].itemTotal =
        parseFloat(tempObj[key].itemTotal) + parseFloat(tempObj[key].itemPrice)

      tempObj[key].itemSubTotal =
        parseFloat(tempObj[key].itemSubTotal) +
        (parseFloat(tempObj[key].itemPrice) -
          parseFloat(tempObj[key].marginAmount) / parseFloat(tempObj[key].quantity))

      tempObj[key].marginAmount =
        parseFloat(tempObj[key].marginAmount) +
        parseFloat(tempObj[key].marginAmount) / parseFloat(tempObj[key].quantity)

      tempObj[key].quantity = tempObj[key].quantity + 1

      subTotal += state.items[key].itemPrice

      colorCharges =
        parseFloat(tempObj[key].quantity) *
        parseFloat(action.payload.selectedColorCharge) *
        parseFloat(state.areaInSqFt)

      tempObj[key].colorCharges = colorCharges

      subTotal = parseFloat(subTotal) + parseFloat(colorCharges)

      if (state.discountValue != '')
        if (state.discountType === 'flat') discount = state.discountValue
        else discount = (parseFloat(state.discountValue) / 100) * parseFloat(subTotal)

      amount = parseFloat(subTotal) - parseFloat(discount)
      if (state.GST != '') GSTAmount = (parseFloat(state.GST) / 100) * parseFloat(amount)
      amount = parseFloat(amount) + parseFloat(GSTAmount)

      return {
        ...state,
        items: tempObj,
        total: amount,
        subTotal: subTotal,
        discountAmount: discount,
        GSTAmount: GSTAmount,
        colorCharges: colorCharges,
      }

    case DECREMENT_ITEM:
      //decrement
      var tempObj = state.items
      var amount = state.total
      var subTotal = state.subTotal
      var discount = 0
      var GSTAmount = 0
      var colorCharges = 0
      var key = action.payload.key

      tempObj[key].itemTotal =
        parseFloat(tempObj[key].itemTotal) - parseFloat(tempObj[key].itemPrice)

      tempObj[key].itemSubTotal =
        parseFloat(tempObj[key].itemSubTotal) -
        (parseFloat(tempObj[key].itemPrice) -
          parseFloat(tempObj[key].marginAmount) / parseFloat(tempObj[key].quantity))

      tempObj[key].marginAmount =
        parseFloat(tempObj[key].marginAmount) -
        parseFloat(tempObj[key].marginAmount) / parseFloat(tempObj[key].quantity)

      subTotal -= state.items[key].itemPrice
      tempObj[key].quantity = tempObj[key].quantity - 1

      colorCharges =
        parseFloat(tempObj[key].quantity) *
        parseFloat(action.payload.selectedColorCharge) *
        parseFloat(state.areaInSqFt)

      subTotal = parseFloat(subTotal) + parseFloat(colorCharges)

      tempObj[key].colorCharges = colorCharges

      if (state.discountValue != '')
        if (state.discountType === 'flat') discount = state.discountValue
        else discount = (parseFloat(state.discountValue) / 100) * parseFloat(subTotal)

      amount = parseFloat(subTotal) - parseFloat(discount)
      if (state.GST != '') GSTAmount = (parseFloat(state.GST) / 100) * parseFloat(amount)
      amount = parseFloat(amount) + parseFloat(GSTAmount)

      return {
        ...state,
        items: tempObj,
        total: amount,
        subTotal: subTotal,
        discountAmount: discount,
        GSTAmount: GSTAmount,
        colorCharges: colorCharges,
      }

    case SET_BILL_ITEMS:
      return {
        ...state,
        items: action.payload.orderDetails,
        total: action.payload.estimatedAmount,
        subTotal: action.payload.subTotal,
        GST: action.payload.GST,
        GSTAmount: action.payload.GSTAmount,
        discountValue: action.payload.discountValue,
        discountType: action.payload.discountType,
        discountAmount: action.payload.discountAmount,
        GSTAmount: action.payload.GSTAmount,
        componentList: action.payload.componentList,
      }

    case SET_COMPONENTS:
      var tempObj = action.payload.componentList
      var subTotal = 0
      var amount = 0
      var marginAmount = 0
      var price = 0
      var colorCharges = 0

      Object.entries(tempObj).map(([key, value]) => {
        if (value.count) {
          value.componentTotal = parseFloat(value.productPrice) * parseFloat(value.count)
          subTotal = parseFloat(subTotal) + parseFloat(value.productPrice) * parseFloat(value.count)
        }
      })

      subTotal = parseFloat(subTotal) * parseFloat(action.payload.quantity)

      colorCharges =
        parseFloat(action.payload.quantity) *
        parseFloat(action.payload.selectedColorCharge) *
        parseFloat(action.payload.areaInSqFt)

      subTotal = parseFloat(subTotal) + parseFloat(colorCharges)

      marginAmount = (parseFloat(action.payload.productMargin) / 100) * parseFloat(subTotal)
      amount = parseFloat(subTotal) + parseFloat(marginAmount)
      price = parseFloat(amount) / parseFloat(action.payload.quantity)

      return {
        ...state,
        componentList: tempObj,
        groupSubTotal: subTotal,
        marginAmount: marginAmount,
        groupTotal: amount,
        productMargin: action.payload.productMargin,
        areaInSqFt: action.payload.areaInSqFt,
        groupPrice: price,
        colorCharges: colorCharges,
      }
    case SET_COMPONENTS_EDIT:
      return {
        ...state,
        componentList: action.payload.componentList,
        groupSubTotal: action.payload.groupSubTotal,
        marginAmount: action.payload.marginAmount,
        groupTotal: action.payload.groupTotal,
        productMargin: action.payload.productMargin,
        areaInSqFt: action.payload.areaInSqFt,
        groupPrice: action.payload.groupPrice,
        colorCharges: action.payload.colorCharges,
      }
    case ADD_COMPONENT:
      //add
      var tempObj = state.componentList
      var subTotal = state.groupSubTotal
      var id = action.payload.value.id
      var item = action.payload.value
      var amount = 0
      var marginAmount = 0
      var price = 0

      tempObj[id] = {}
      tempObj[id].id = id
      tempObj[id].productName = item.productName
      tempObj[id].productDescription = item.productDescription
      tempObj[id].productImages = item.productImages
      tempObj[id].unitOfMeasurement = item.unitOfMeasurement
      tempObj[id].productPrice = item.productPrice
      tempObj[id].componentTotal = parseFloat(item.productPrice)
      tempObj[id].count = 1
      if (item.calBasedOn) tempObj[id].calBasedOn = item.calBasedOn
      if (item.minValue) tempObj[id].minValue = item.minValue
      if (item.unitPerPanel) tempObj[id].unitPerPanel = item.unitPerPanel

      subTotal = subTotal + parseFloat(item.productPrice) * parseFloat(action.payload.quantity)
      marginAmount = (parseFloat(state.productMargin) / 100) * parseFloat(subTotal)

      amount = parseFloat(subTotal) + parseFloat(marginAmount)
      price = parseFloat(amount) / parseFloat(action.payload.quantity)

      return {
        ...state,
        componentList: tempObj,
        groupSubTotal: subTotal,
        marginAmount: marginAmount,
        groupTotal: amount,
        groupPrice: price,
      }
    case REMOVE_COMPONENT:
      var tempObj = state.componentList
      var subTotal = state.groupSubTotal
      var amount = 0
      var marginAmount = 0
      var price = 0

      subTotal -=
        tempObj[action.payload.id].count *
        tempObj[action.payload.id].productPrice *
        parseFloat(action.payload.quantity)
      delete tempObj[action.payload.id]

      marginAmount = (parseFloat(state.productMargin) / 100) * parseFloat(subTotal)

      amount = parseFloat(subTotal) + parseFloat(marginAmount)
      price = parseFloat(amount) / parseFloat(action.payload.quantity)

      return {
        ...state,
        componentList: tempObj,
        groupSubTotal: subTotal,
        marginAmount: marginAmount,
        groupTotal: amount,
        groupPrice: price,
      }

    case CHANGE_COM_COUNT:
      var tempObj = state.componentList
      var subTotal = state.groupSubTotal
      var amount = 0
      var marginAmount = 0
      var price = 0
      if (action.payload.count != '' && action.payload.count != '.') {
        subTotal =
          subTotal -
          parseFloat(tempObj[action.payload.id].componentTotal) *
            parseFloat(action.payload.quantity)

        tempObj[action.payload.id].count = action.payload.count
        tempObj[action.payload.id].componentTotal =
          parseFloat(state.componentList[action.payload.id].productPrice) *
          parseFloat(action.payload.count)
        subTotal =
          subTotal +
          parseFloat(state.componentList[action.payload.id].productPrice) *
            parseFloat(action.payload.count) *
            parseFloat(action.payload.quantity)
      } else {
        subTotal =
          subTotal -
          parseFloat(tempObj[action.payload.id].componentTotal) *
            parseFloat(action.payload.quantity)
        tempObj[action.payload.id].count = action.payload.count
        tempObj[action.payload.id].componentTotal = 0
      }

      marginAmount = (parseFloat(state.productMargin) / 100) * parseFloat(subTotal)
      amount = parseFloat(subTotal) + parseFloat(marginAmount)
      price = parseFloat(amount) / parseFloat(action.payload.quantity)

      return {
        ...state,
        componentList: tempObj,
        groupSubTotal: subTotal,
        marginAmount: marginAmount,
        groupTotal: amount,
        groupPrice: price,
      }

    case SET_EXTRA_CHARGES:
      var amount = state.total
      var subTotal = state.subTotal
      var discount = 0
      var GSTAmount = 0
      if (action.payload.discountValue != '')
        if (action.payload.discountType === 'flat') discount = action.payload.discountValue
        else discount = (action.payload.discountValue / 100) * subTotal
      amount = parseFloat(subTotal) - parseFloat(discount)

      if (action.payload.GST != '')
        GSTAmount = (parseFloat(action.payload.GST) / 100) * parseFloat(amount)
      amount = parseFloat(amount) + parseFloat(GSTAmount)

      return {
        ...state,
        total: amount,
        subTotal: subTotal,
        GST: action.payload.GST,
        GSTAmount: GSTAmount,
        discountValue: action.payload.discountValue,
        discountType: action.payload.discountType,
        discountAmount: discount,
      }

    case SET_AEAR_SQFT:
      var width = action.payload.width * 0.00328084
      var height = action.payload.height * 0.00328084
      var areaInSqFt = width * height

      return {
        ...state,
        areaInSqFt: parseFloat(areaInSqFt).toFixed(2),
      }
    case COLOR_CHANGED:
      var subTotal = state.groupSubTotal
      var amount = 0
      var marginAmount = 0
      var price = 0

      colorCharges =
        parseFloat(action.payload.quantity) *
        parseFloat(action.payload.selectedColorCharge) *
        parseFloat(state.areaInSqFt)

      subTotal = parseFloat(subTotal) - parseFloat(state.colorCharges) + parseFloat(colorCharges)

      marginAmount = (parseFloat(state.productMargin) / 100) * parseFloat(subTotal)
      amount = parseFloat(subTotal) + parseFloat(marginAmount)
      price = parseFloat(amount) / parseFloat(action.payload.quantity)

      return {
        ...state,
        groupSubTotal: subTotal,
        marginAmount: marginAmount,
        groupTotal: amount,
        groupPrice: price,
        colorCharges: colorCharges,
      }

    case COLOR_CHARGES_CHANGED:
      var subTotal = state.groupSubTotal
      var amount = 0
      var marginAmount = 0
      var price = 0
      var colorCharges = 0

      if (action.payload.colorCharges != '' && action.payload.colorCharges != '.') {
        colorCharges = action.payload.colorCharges
      } else {
        colorCharges = 0
      }

      subTotal = parseFloat(subTotal) - parseFloat(state.colorCharges) + parseFloat(colorCharges)

      marginAmount = (parseFloat(state.productMargin) / 100) * parseFloat(subTotal)
      amount = parseFloat(subTotal) + parseFloat(marginAmount)
      price = parseFloat(amount) / parseFloat(action.payload.quantity)

      return {
        ...state,
        groupSubTotal: subTotal,
        marginAmount: marginAmount,
        groupTotal: amount,
        groupPrice: price,
        colorCharges: colorCharges,
      }

    default:
      return state
  }
}
