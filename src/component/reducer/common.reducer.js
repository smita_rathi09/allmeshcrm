import { SET_JSON, SET_USERDATA } from '../action'

const initialState = {
  username: '',
  preferred_username: '',
  name: '',
  orderStatusArray: [],
  groupArray: [],
  roleArray: [],
  UOMArray: [],
  calBasedOn: [],
  role: '',
  sales: [],
  admin: [],
  manager: [],
  owner: [],
  color: {},
  productLocationTypeMap: {},
}

export default function cartReducer(state = initialState, action) {
  switch (action.type) {
    case SET_USERDATA:
      return {
        ...state,
        username: action.payload.username,
        preferred_username: action.payload.preferred_username,
        name: action.payload.name,
        role: action.payload.role,
      }
    case SET_JSON:
      return {
        ...state,
        orderStatusArray: action.payload.orderStatusArray,
        groupArray: action.payload.groupArray,
        roleArray: action.payload.roleArray,
        UOMArray: action.payload.UOMArray,
        calBasedOn: action.payload.calBasedOn,
        sales: action.payload.sales,
        admin: action.payload.admin,
        manager: action.payload.manager,
        owner: action.payload.owner,
        color: action.payload.color,
        productLocationTypeMap: action.payload.productLocationTypeMap,
      }
    default:
      return state
  }
}
