import { combineReducers } from 'redux'

import commonReducer from './common.reducer'
import billReducer from './billing.reducer'

const rootReducer = combineReducers({
  common: commonReducer,
  bill: billReducer,
})

// export default rootReducer

export default (state, action) =>
  rootReducer(action.type === 'USER_LOGOUT' ? undefined : state, action)
