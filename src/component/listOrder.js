import { useNavigate, Link } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import { API, Auth } from 'aws-amplify'
import '../styles/listQuotations.css'
import { DataGrid } from '@mui/x-data-grid'
import * as queries from '../graphql/queries'
import Header from './header'
import { connect } from 'react-redux'
import moment from 'moment'

function ListOrder(props) {
  const navigate = useNavigate()
  const [allOrder, setAllOrder] = useState([])
  const [status, setStatus] = useState('')

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        getOrders()
      })
      .catch((err) => {
        console.log('err ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.sales.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getOrders = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getAllItemsByType,
        variables: { type: 'Order' },
      })
      setAllOrder(
        allTodos.data.getAllItemsByType
          .filter((item) => item.status != 'deleted')
          .sort(
            (a, b) =>
              moment(b.addedDate + ' ' + b.time, 'YYYY-MM-DD  HH:mm:ss') -
              moment(a.addedDate + ' ' + a.time, 'YYYY-MM-DD  HH:mm:ss'),
          ),
      )
    } catch (error) {
      console.log('error', JSON.stringify(error))
    }
  }

  return (
    <div>
      <Header />
      <div>
        <div>
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
            <p className="listQuoteTitle">
              You have{' '}
              {status == ''
                ? allOrder.length
                : allOrder.filter((item) => item.status === status).length}{' '}
              order(s)
            </p>

            <div>
              <span>Select status: </span>
              <select
                value={status}
                onChange={(e) => {
                  setStatus(e.target.value)
                  getOrders(e.target.value)
                }}
              >
                <option value="">Select</option>
                <option value="live">New</option>;
                <option value="inProduction">In-Production</option>;
                <option value="outForDelivery">Out for Delivery</option>;
                <option value="delivered">Delivered</option>;
                <option value="installed">Installed</option>;
                <option value="rejected">Rejected</option>;
              </select>
            </div>
          </div>
          <p></p>

          <div style={{ height: 600, width: '100%' }}>
            <DataGrid
              hover
              // rows={allOrder}
              rows={status == '' ? allOrder : allOrder.filter((item) => item.status === status)}
              columns={[
                {
                  field: 'customerName',
                  headerName: 'Customer Name',
                  width: 250,
                },
                {
                  field: 'customerPhone',
                  headerName: 'Phone Number',
                  width: 130,
                },
                { field: 'customerEmail', headerName: 'Email', width: 220 },
                {
                  field: 'addedDate',
                  valueFormatter: (params) => {
                    return moment(params.value, 'YYYY-MM-DD').format('D MMM')
                  },
                  headerName: 'Order Date',
                  width: 120,
                },
                {
                  field: 'time',
                  valueFormatter: (params) => {
                    return moment(params.value, 'HH:mm:ss').format('hh:mm A')
                  },
                  headerName: 'Order Time',
                  width: 120,
                },
                {
                  field: 'estimatedAmount',
                  valueFormatter: (params) => {
                    return '₹ ' + params.value.toFixed(2)
                  },
                  headerName: 'Amount (₹)',
                  width: 120,
                },
                {
                  field: 'status',
                  valueFormatter: (params) => {
                    return params.value == 'live'
                      ? 'Live'
                      : params.value == 'inProduction'
                      ? 'In Production'
                      : params.value == 'outForDelivery'
                      ? 'Out For Delivery'
                      : params.value == 'delivered'
                      ? 'Delivered'
                      : params.value == 'installed'
                      ? 'Installed'
                      : params.value == 'rejected'
                      ? 'Rejected'
                      : 'Others'
                  },
                  headerName: 'Status',
                  width: 160,
                },
                {
                  field: 'id',
                  headerName: 'View Order',
                  width: 120,
                  renderCell: (id) => {
                    return (
                      <>
                        <Link to={`../ViewQuotation/${id.row.id}/order`} className="editButton">
                          View
                        </Link>
                      </>
                    )
                  },
                },
              ]}
              pageSize={8}
              rowsPerPageOptions={[8]}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}
function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(ListOrder)
