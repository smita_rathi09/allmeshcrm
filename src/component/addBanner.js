import { useNavigate, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import '../styles/addBanner.css'
import { Auth, API } from 'aws-amplify'
import * as mutations from '../graphql/mutations'
import * as queries from '../graphql/queries'
import Header from './header'
import environment from '../configure'
import { connect } from 'react-redux'
// const uuidv4 = require('uuid/v4')
import {v4 as uuidv4} from 'uuid';

function AddBanner(props) {
  const navigate = useNavigate()
  const [text1, setText1] = useState('')
  const [text2, setText2] = useState('')
  const [text3, setText3] = useState('')
  const [addedBy, setAddedBy] = useState('')
  const [updatedBy, setupdatedBy] = useState('')
  const [bannerImages, setBannerImages] = useState([])
  const [bannerImagesURI, setBannerImagesURI] = useState('')
  const [bannerImagesBase64, setBannerImagesBase64] = useState('')
  const [spinnerState, setSpinnerState] = useState(false)

  let test = useParams()

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        setAddedBy(user.username)
        setupdatedBy(user.username)
        if (test.test != 'addBanner') getItemById()
      })
      .catch((err) => {
        console.log('currentAuthenticatedUser ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.manager.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemById = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemById,
        variables: { id: test.test },
      })
      // console.log(allTodos.data.getItemById)
      let res = allTodos.data.getItemById
      if (res == null) navigate('/listBanners')
      else {
        setText1(res.text1)
        setText2(res.text2)
        setText3(res.text3)
        setBannerImages(res.productImages)
        setBannerImagesURI(res.productImages)
      }
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }
  const addItem = async (image) => {
    try {
      await API.graphql({
        query: mutations.addItem,
        variables: {
          text1: text1,
          text2: text2,
          text3: text3,
          addedBy: addedBy,
          productImages: image ?? bannerImages,
          type: 'Banner',
        },
      })
      alert('Banner Added')
      navigate('/listBanners')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const editItem = async (image) => {
    try {
      await API.graphql({
        query: mutations.editItem,
        variables: {
          id: test.test,
          text1: text1,
          text2: text2,
          text3: text3,
          updatedBy: updatedBy,
          productImages: image ?? bannerImages,
        },
      })
      alert('Banner updated')
      navigate('/listBanners')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const checkInput = async () => {
    if (text1 == '') return alert('Please enter text1')
    if (text2 == '') return alert('Please enter text2')
    if (test.test === 'addBanner' && bannerImagesURI.length == 0)
      return alert('Please select an image')

    var tempImage = undefined
    if (
      (test.test === 'addBanner' && bannerImagesURI.length > 0) ||
      (test.test != 'addBanner' && bannerImagesURI != bannerImages)
    )
      try {
        const response = await fetch(environment.apiGateway + 'uploadImage', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            base64: bannerImagesBase64,
            fileName: uuidv4() + '.jpg',
          }),
        })
        const responseJson = await response.json()
        tempImage = responseJson.Location
      } catch (error) {
        console.error(error)
      }
    setSpinnerState('true')
    if (test.test === 'addBanner') addItem(tempImage)
    else editItem(tempImage)
  }

  const onImageChange = (event) => {
    let files = event.target.files
    let reader = new FileReader()
    reader.readAsDataURL(files[0])
    setBannerImagesURI(URL.createObjectURL(files[0]))
    reader.onload = (e) => {
      setBannerImagesBase64(e.target.result.replace(/^data:image\/\w+;base64,/, ''))
    }
  }

  return (
    <div>
      <Header />
      <div>
        <div>
          <p className="addBannerTitle">Enter Banner Details</p>
        </div>
        <div style={{ width: '400px', marginLeft: '20px' }}>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Text 1: </span>
            <input
              value={text1}
              className="addBannerInputStyle"
              onChange={(e) => setText1(e.target.value)}
              maxLength={200}
            />
          </div>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Text 2: </span>
            <input
              value={text2}
              className="addBannerInputStyle"
              onChange={(e) => setText2(e.target.value)}
              maxLength={500}
            />
          </div>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Text 3: </span>
            <input
              value={text3}
              className="addBannerInputStyle"
              onChange={(e) => setText3(e.target.value)}
              maxLength={200}
            />
          </div>

          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName checkBoxNames" style={{ marginRight: 30 }}>
              Banner Image
            </span>
            <input type="file" name="myImage" onChange={onImageChange} />
          </div>
          <div style={{ textAlign: 'right' }}>
            <img src={bannerImagesURI} width={150} />
          </div>

          <p />
          <div className="addBannerFieldStyle">
            <button disabled={spinnerState} onClick={() => checkInput()} className="addProductBtn">
              {test.test === 'addBanner' ? `Add Banner` : `Update Banner`}
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}

function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(AddBanner)
