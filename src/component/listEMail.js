import { useNavigate, Link } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import '../styles/listEmails.css'
import { Auth, API } from 'aws-amplify'
import * as queries from '../graphql/queries'
import * as mutations from '../graphql/mutations'
import { AiFillDelete } from 'react-icons/ai'
import Header from './header'
import { RiDeleteBin6Line } from 'react-icons/ri'
import { connect } from 'react-redux'

function ListEmail(props) {
  const navigate = useNavigate()
  const [allEmails, setAllEmails] = useState([])
  const [term, setTerm] = useState('')

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then(async (user) => {
        await manageEmail()
      })
      .catch((err) => {
        console.log('err ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.manager.includes(props.common.role)) navigate('/homePage')
  }, [])

  const manageEmail = async () => {
    try {
      const allTodos = await API.graphql({
        query: mutations.manageEmail,
        variables: { type: 'Email', action: 'listEmail' },
      })
      setAllEmails(JSON.parse(allTodos.data.manageEmail.output))
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const deleteItem = async (email, id) => {
    try {
      const allTodos = await API.graphql({
        query: mutations.manageEmail,
        variables: { email: email, id: id, action: 'deleteEmail' },
      })
      alert('E-mail Deleted')
      manageEmail()
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  return (
    <div>
      <Header />
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <p className="header">{allEmails.length} E-Mail(s) added by you</p>
          <input
            className="searchInputBox"
            value={term}
            onChange={(e) => setTerm(e.target.value)}
            placeholder="search by e-mail"
          />
          <button className="addNewProdButtonContainer">
            <Link to="../AddEMail/addEMail" className="addNewProdButton">
              Add New E-Mail
            </Link>
          </button>
        </div>
        <div className="productRow productRowHeading">
          <p style={{ width: 220, fontSize: 18 }}>E-mail</p>
          <p style={{ width: 100, fontSize: 18 }}>Name</p>
          <p style={{ width: 60, fontSize: 18 }}>Status</p>
          <p style={{ width: 70, fontSize: 18 }}>Group</p>
          <p style={{ width: 70, fontSize: 18 }}>Role</p>

          <p style={{ width: 30, fontSize: 18 }}>Edit</p>
          <p style={{ width: 30, fontSize: 18 }}>Delete</p>
        </div>

        {(term == ''
          ? allEmails
          : allEmails.filter(
              (item) => item.email.toString().toLowerCase().indexOf(term.toLowerCase()) > -1,
            )
        ).map((item) => {
          return (
            <>
              <div className="productRow">
                <p style={{ width: 220 }}>{item.email}</p>
                <p style={{ width: 100 }}>{item.name}</p>
                <p style={{ width: 60 }}>{item.verificationStatus}</p>
                <p style={{ width: 70 }}>{item.group}</p>
                <p style={{ width: 70 }}>{item.role}</p>

                <span style={{ width: 30 }}>
                  <button className="editButtonContainer">
                    <Link to={`../AddEMail/${item.id}`} className="editButton">
                      Edit
                    </Link>
                  </button>
                </span>
                <span style={{ width: 30 }}>
                  <button onClick={() => deleteItem(item.email, item.id)} className="deleteIcon">
                    <RiDeleteBin6Line />
                  </button>
                </span>
              </div>
            </>
          )
        })}
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}

function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(ListEmail)
