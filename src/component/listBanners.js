import { useNavigate, Link } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import '../styles/listBanners.css'
import { Auth, API } from 'aws-amplify'
import * as queries from '../graphql/queries'
import * as mutations from '../graphql/mutations'
import { AiFillDelete } from 'react-icons/ai'
import Header from './header'
import { connect } from 'react-redux'

function ListBanners(props) {
  const navigate = useNavigate()
  const [allProducts, SetAllProducts] = useState([])
  const [term, setTerm] = useState('')

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        getItemsByType()
      })
      .catch((err) => {
        console.log('err ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.manager.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemsByType = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemsByType,
        variables: { type: 'Banner', status: 'live' },
      })
      // console.log(allTodos.data.getItemsByType)
      // SetAllProducts(allTodos);
      SetAllProducts(allTodos.data.getItemsByType)
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const deleteItem = async (id) => {
    try {
      await API.graphql({
        query: mutations.deleteItem,
        variables: { id: id },
      })
      alert('Banner Deleted')
      getItemsByType()
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  return (
    <div>
      <Header />
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <p className="header">{allProducts.length} banner(s) added by you</p>
          <input
            className="searchInputBox"
            value={term}
            onChange={(e) => setTerm(e.target.value)}
            placeholder="search by text 1"
          />
          <button className="addNewProdButtonContainer">
            <Link to="../AddBanner/addBanner" className="addNewProdButton">
              Add New Banner
            </Link>
          </button>
        </div>
        <div className="productRow productRowHeading">
          <p style={{ width: 100, fontSize: 18 }}>Image</p>
          <p style={{ width: 250, fontSize: 18 }}>Text 1</p>
          <p style={{ width: 350, fontSize: 18 }}>Text 2</p>
          <p style={{ width: 100, fontSize: 18 }}>Text 3</p>
          <p style={{ width: 40, fontSize: 18 }}>Edit</p>
          <p style={{ width: 40, fontSize: 18 }}>Delete</p>
        </div>
        {(term == ''
          ? allProducts
          : allProducts.filter(
              (item) => item.text1.toString().toLowerCase().indexOf(term.toLowerCase()) > -1,
            )
        ).map((item) => {
          return (
            <>
              <div className="productRow">
                <div style={{ paddingBottom: '5px', paddingTop: '5px', width: 100 }}>
                  <img src={item.productImages} height={80} width={100} />
                </div>
                <p style={{ width: 250 }}>{item.text1}</p>
                <p style={{ width: 350 }}> {item.text2}</p>
                <p style={{ width: 100 }}> {item.text3}</p>
                <span style={{ width: 40 }}>
                  <button>
                    <Link to={`../AddBanner/${item.id}`} className="editButton">
                      Edit
                    </Link>
                  </button>
                </span>
                <span style={{ width: 40 }}>
                  <button onClick={() => deleteItem(item.id)} className="deleteIcon">
                    <AiFillDelete />
                  </button>
                </span>
              </div>
            </>
          )
        })}
      </div>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}
function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(ListBanners)
