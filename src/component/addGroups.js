import { useNavigate, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import '../styles/addProducts.css'
import { Auth, API } from 'aws-amplify'
import * as mutations from '../graphql/mutations'
import * as queries from '../graphql/queries'
import moment from 'moment'
import uuidv4 from 'uuid/dist/v4'
import Header from './header'
import { useSelector, useDispatch } from 'react-redux'
import Modal from 'react-modal'
import { AiOutlineCloseCircle } from 'react-icons/ai'
import { connect } from 'react-redux'
import environment from '../configure'

function AddProductGroups(props) {
  const navigate = useNavigate()
  const [productLocation, setProductLocation] = useState('')
  const [productType, setProductType] = useState('')
  const [productSubType, setProductSubType] = useState('')
  const [productName, setProductName] = useState('')
  const [productDescription, setProductDescription] = useState('')
  const [addedBy, setAddedBy] = useState('')
  const [updatedBy, setupdatedBy] = useState('')
  const [productImages, setProductImages] = useState([])
  const [productImagesURI, setProductImagesURI] = useState('')
  const [productImagesBase64, setProductImagesBase64] = useState('')
  const [spinnerState, setSpinnerState] = useState(false)
  const [allProducts, setAllProducts] = useState([])
  const [componentList, setComponentList] = useState({})
  const [modalAddComponents, setModalAddComponents] = useState(false)
  const [productMargin, setProductMargin] = useState('')
  const [term, setTerm] = useState('')

  let test = useParams()

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        setAddedBy(user.username)
        setupdatedBy(user.username)
        if (test.test != 'addNewGroup') getItemById()
        getItemsByType()
      })
      .catch((err) => {
        console.log('currentAuthenticatedUser ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.admin.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemById = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemById,
        variables: { id: test.test },
      })
      // console.log(allTodos.data.getItemById)
      let res = allTodos.data.getItemById
      if (res == null) navigate('/ListGroups')
      else {
        setProductName(res.productName)
        setProductDescription(res.productDescription)
        setProductImages(res.productImages)
        setProductImagesURI(res.productImages)
        setComponentList(JSON.parse(res.componentList))
        setProductLocation(res.productLocation)
        setProductType(res.productType)
        setProductSubType(res.productSubType)
        setProductMargin(res.productMargin)
      }
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }
  const getItemsByType = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemsByType,
        variables: { type: 'Component', status: 'live' },
      })
      // console.log(allTodos.data.getItemsByType)
      setAllProducts(allTodos.data.getItemsByType)
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }
  const addItem = async (image) => {
    try {
      await API.graphql({
        query: mutations.addItem,
        variables: {
          productName: productName,
          productDescription: productDescription,
          addedBy: addedBy,
          productImages: image ?? productImages,
          type: 'Group',
          componentList: JSON.stringify(componentList),
          productType: productType,
          productSubType: productSubType,
          productLocation: productLocation,
          productMargin: productMargin,
        },
      })
      alert('Group Added')
      navigate('/ListGroups')
    } catch (error) {
      console.log(JSON.stringify(error))
      alert(JSON.stringify(error))
    }
  }

  const editItem = async (image) => {
    try {
      await API.graphql({
        query: mutations.editItem,
        variables: {
          id: test.test,
          productName: productName,
          productDescription: productDescription,
          updatedBy: updatedBy,
          productImages: image ?? productImages,
          componentList: JSON.stringify(componentList),
          productType: productType,
          productSubType: productSubType,
          productLocation: productLocation,
          productMargin: productMargin,
        },
      })
      alert('Group updated')
      navigate('/ListGroups')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const checkInput = async () => {
    if (productLocation === '') return alert('Please select product location')
    if (productType === '') return alert('Please select product type')
    if (productName === '') return alert('Please enter product name')
    if (productDescription === '') return alert('Please enter product description')
    if (productMargin === '') return alert('Please enter product margin')
    if (isNaN(productMargin)) return alert('Please enter valid margin')
    if (Object.keys(componentList).length == 0)
      return alert('Please add components to the product group')

    var tempImage = undefined

    if (
      (test.test === 'addNewGroup' && productImagesURI.length > 0) ||
      (test.test != 'addNewGroup' && productImagesURI != productImages)
    )
      try {
        const response = await fetch(environment.apiGateway + 'uploadImage', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            base64: productImagesBase64,
            fileName: uuidv4() + '.jpg',
          }),
        })
        const responseJson = await response.json()
        tempImage = responseJson.Location
      } catch (error) {
        console.error(error)
      }

    setSpinnerState('true')
    if (test.test === 'addNewGroup') addItem(tempImage)
    else editItem(tempImage)
  }

  const onImageChange = (event) => {
    let files = event.target.files
    let reader = new FileReader()
    reader.readAsDataURL(files[0])
    setProductImagesURI(URL.createObjectURL(files[0]))
    reader.onload = (e) => {
      setProductImagesBase64(e.target.result.replace(/^data:image\/\w+;base64,/, ''))
    }
  }

  const addComponent = (item) => {
    let tempList = { ...componentList }
    if (Object.keys(tempList).includes(item.id)) {
      delete tempList[item.id]
    } else {
      tempList[item.id] = {}
      tempList[item.id].id = item.id
      tempList[item.id].productName = item.productName
    }
    setComponentList(tempList)
  }

  const customStylesAddProducts = {
    content: {
      top: '50%',
      width: '1100px',
      height: '600px',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  }

  return (
    <div>
      <Header />
      <div>
        <div>
          <p className="addProductTitle">Enter Product Group Details</p>
        </div>
        <div className="addProductFormContainer">
          <div className="fieldStyle">
            <span className="fieldName">Product Location:</span>
            <select
              className="addProdSelectStyle"
              value={productLocation}
              onChange={(e) => {
                setProductLocation(e.target.value)
                setProductType('')
                setProductSubType('')
              }}
            >
              <option value="">Select</option>;
              {Object.entries(props.common.productLocationTypeMap).map(([key, value]) => {
                return <option value={key}>{key}</option>
              })}
            </select>
          </div>
          {productLocation !== '' && (
            <div className="fieldStyle">
              <span className="fieldName">Product Type:</span>
              <select
                className="addProdSelectStyle"
                value={productType}
                onChange={(e) => {
                  setProductType(e.target.value)
                  setProductSubType('')
                }}
              >
                <option value="">Select</option>;
                {Object.entries(props.common.productLocationTypeMap[productLocation].value).map(
                  ([key, value]) => {
                    return <option value={key}>{key}</option>
                  },
                )}
              </select>
            </div>
          )}
          {productType !== '' &&
            Object.entries(
              props.common.productLocationTypeMap[productLocation].value[productType].value,
            ).length > 0 && (
              <div className="fieldStyle">
                <span className="fieldName">Product Sub-Type:</span>
                <select
                  className="addProdSelectStyle"
                  value={productSubType}
                  onChange={(e) => setProductSubType(e.target.value)}
                >
                  <option value="">Select</option>;
                  {Object.entries(
                    props.common.productLocationTypeMap[productLocation].value[productType].value,
                  ).map((value) => {
                    return <option value={value[1].name}>{value[1].name}</option>
                  })}
                </select>
              </div>
            )}
          <div className="fieldStyle">
            <span className="fieldName">Product Name:</span>
            <input
              className="addProdInputStyle"
              value={productName}
              onChange={(e) => setProductName(e.target.value)}
              maxLength={200}
            />
          </div>
          <div className="fieldStyle">
            <span className="fieldName">Product Description: </span>
            <input
              className="addProdInputStyle"
              value={productDescription}
              onChange={(e) => setProductDescription(e.target.value)}
              maxLength={200}
            />
          </div>
          <div className="fieldStyle">
            <span className="fieldName">Product Margin(%): </span>
            <input
              className="addProdInputStyle"
              value={productMargin}
              onChange={(e) => setProductMargin(e.target.value)}
              maxLength={4}
            />
          </div>
          <div className="fieldStyle">
            <span className="fieldName checkBoxNames" style={{ marginRight: 45 }}>
              Product Image
            </span>
            <input type="file" name="myImage" onChange={onImageChange} accept="image/*" />
          </div>
          <img src={productImagesURI} width={150} style={{ marginLeft: '200px' }} />
          <div className="fieldStyle">
            <span className="fieldName checkBoxNames">Components: </span>
            <div
              style={{
                // display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                width: '200px',
              }}
            >
              {Object.entries(componentList).map(([key, value]) => {
                return <div>{value.productName}</div>
              })}
            </div>
          </div>

          <div className="fieldStyle">
            <button
              disabled={spinnerState}
              onClick={() => setModalAddComponents(true)}
              className="addProductBtn"
            >
              Add Components
            </button>
            <button disabled={spinnerState} onClick={() => checkInput()} className="addProductBtn">
              {test.test === 'addNewGroup' ? `Add Group` : `Update Group`}
            </button>
          </div>
        </div>
      </div>
      <Modal isOpen={modalAddComponents} style={customStylesAddProducts}>
        <div>
          <div className="closeButtonContainer">
            <button className="closeButton" onClick={() => setModalAddComponents(false)}>
              <AiOutlineCloseCircle />
            </button>
          </div>

          <input
            className="searchInputBox"
            value={term}
            onChange={(e) => setTerm(e.target.value)}
            placeholder="search by name"
          />
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p style={{ fontWeight: 'bold', width: 120 }}>Component Name</p>
            <p style={{ fontWeight: 'bold', width: 70 }}>Description</p>
            <p style={{ fontWeight: 'bold', width: 70 }}></p>
          </div>

          {(term == ''
            ? allProducts
            : allProducts.filter(
                (item) =>
                  item.productName.toString().toLowerCase().indexOf(term.toLowerCase()) > -1,
              )
          ).map((item) => {
            return (
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  borderBottom: '1px solid gray',
                  padding: '10px 0px',
                }}
              >
                <p style={{ width: 120 }}>{item.productName}</p>
                <p style={{ width: 70 }}>{item.productDescription}</p>
                <div style={{ width: 70 }}>
                  <button className="addProductBtn" onClick={() => addComponent(item)}>
                    {Object.keys(componentList).includes(item.id) ? 'Remove' : 'Add'}
                  </button>
                </div>
              </div>
            )
          })}
          <div className="fieldStyle">
            <button className="addProductBtn" onClick={() => setModalAddComponents(false)}>
              Add Components
            </button>
          </div>
        </div>
      </Modal>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}

function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(AddProductGroups)
