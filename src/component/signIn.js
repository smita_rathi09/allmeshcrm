import { useNavigate, Link } from 'react-router-dom'
import React, { useState } from 'react'
import '../styles/signIn.css'
import { Auth, API } from 'aws-amplify'
import { createUserDetails } from '../graphql/mutations'

export default function SignIn() {
  const navigate = useNavigate()
  const [username, setUserName] = useState('')
  const [password, setPassword] = useState('')
  const [preferred_username, setPreferredUserName] = useState('preferred_username')
  const [name, setName] = useState('NA')
  const [role, setRole] = useState('Sales')
  const [spinnerState, setSpinnerState] = useState(false)

  const signInUser = async () => {
    try {
      const user = await Auth.signIn(username, password)
      // console.log(JSON.stringify(user))
      {
        try {
          await API.graphql({
            query: createUserDetails,
            variables: {
              username: username,
              preferred_username: preferred_username,
              name: name,
              role: role,
            },
          })
          // alert('no error')
          navigate('../homePage')
        } catch (error) {
          // console.log(JSON.stringify(error))
          if (error.errors[0].errorType === 'DynamoDB:ConditionalCheckFailedException')
            navigate('../homePage')
          // alert(JSON.stringify(error))
        }
      }
    } catch (error) {
      if (error.code === 'UserNotConfirmedException')
        alert('User is not approved, please contact admin')
      if (error.code === 'NotAuthorizedException') alert('Incorrect username or password.')
      if (error.code === 'UserNotFoundException')
        alert('You do not have an account, please sign-up.')

      console.log('error signing in', error)
    }
  }

  const checkInput = () => {
    if (username === '') return alert('Please enter user name')
    if (password === '') return alert('Please enter password')
    signInUser()
  }

  return (
    <div style={{ background: '#f3f3f3' }}>
      <p className="welcomeTitle">Welcome to AllMesh CRM Portal</p>
      <div className="mainContainer">
        <div className="signInDiv">
          <p className="title1">Enter your details to login</p>
          <div className="credentialsDiv">
            <div className="phnNumIconDiv">
              <p className="userNamePassWord">User Name</p>
            </div>
            <div className="phnNumDiv">
              <div>
                <span className="phnNumCont">
                  <input value={username} onChange={(e) => setUserName(e.target.value)} />
                </span>
              </div>
            </div>
            <div className="phnNumIconDiv">
              <p className="userNamePassWord">Password</p>
            </div>
            <div className="phnNumDiv">
              <div>
                <span className="phnNumCont">
                  <input
                    value={password}
                    type="password"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </span>
              </div>
            </div>
            <div className="buttonDiv">
              <button disabled={spinnerState} className="button" onClick={() => checkInput()}>
                Log In
              </button>
              <p>
                Don’t have an account?{' '}
                <span>
                  <Link to="SignUp" className="signUpBtn">
                    Sign-Up
                  </Link>
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
