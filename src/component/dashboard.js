import { useNavigate } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import '../styles/dashBoard.css'
import { Auth, API } from 'aws-amplify'
import * as queries from '../graphql/queries'
import moment from 'moment'

export default function Dashboard() {
  const navigate = useNavigate()
  const [allQuotation, setAllQuotation] = useState([])
  const [allScheduled, setAllScheduled] = useState([])
  const [allOrders, setAllOrders] = useState([])
  const [allDeliveryDates, setAllDeliveryDates] = useState([])
  const [today, setToday] = useState(moment(new Date()))

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        getAllQuotations()
        getAllScheduled()
        getAllOrders()
      })
      .catch((err) => {
        console.log('err ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
  }, [])

  const getAllQuotations = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getAllItemsByType,
        variables: { type: 'Quotation' },
      })
      setAllQuotation(allTodos.data.getAllItemsByType)
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const getAllScheduled = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemsByType,
        variables: { type: 'Quotation', status: 'visitScheduled' },
      })
      setAllScheduled(allTodos.data.getItemsByType)
    } catch (error) {
      console.log('error', JSON.stringify(error))
    }
  }

  const getAllOrders = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getAllItemsByType,
        variables: { type: 'Order' },
      })
      setAllOrders(allTodos.data.getAllItemsByType)
      setAllDeliveryDates(allTodos.data.getAllItemsByType.filter((item) => item.estimatedDelivery))
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  return (
    <div>
      <div>
        {/* <p className="dashboardTitle">Dashboard</p> */}

        <p className="dashboardTitle">Quotations</p>
        <div className="tablesMainContainer">
          <div className="tableIndividualContainer">
            <p className="tableTitle">Quotations by status</p>
            <table>
              <tbody>
                <tr>
                  <th className="tableHeaders">Status</th>
                  <th className="tableHeaders">Count</th>
                </tr>
                <tr>
                  <td className="tableContents">New Quotation</td>
                  <td className="tableContents">
                    {allQuotation.filter((item) => item.status === 'live').length}
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Scheduled Visit</td>
                  <td className="tableContents">
                    {allQuotation.filter((item) => item.status === 'visitScheduled').length}
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Converted to order</td>
                  <td className="tableContents">
                    {allQuotation.filter((item) => item.status === 'convertedToOrder').length}
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Rejected</td>
                  <td className="tableContents">
                    {allQuotation.filter((item) => item.status === 'rejected').length}
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Expired</td>
                  <td className="tableContents">
                    {allQuotation.filter((item) => item.status === 'expired').length}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="tableIndividualContainer">
            <p className="tableTitle">Scheduled Customer Visit</p>
            <table>
              <tbody>
                <tr>
                  <th className="tableHeaders">Status</th>
                  <th className="tableHeaders">Count</th>
                </tr>
                <tr>
                  <td className="tableContents">Past Due</td>
                  <td className="tableContents">
                    {
                      allScheduled.filter(
                        (item) =>
                          moment(item.visitDate).diff(today.format('YYYY-MM-DD'), 'days') < 0,
                      ).length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Today</td>
                  <td className="tableContents">
                    {
                      allScheduled.filter(
                        (item) =>
                          moment(item.visitDate).diff(today.format('YYYY-MM-DD'), 'days') == 0,
                      ).length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Tomorrow</td>
                  <td className="tableContents">
                    {
                      allScheduled.filter(
                        (item) =>
                          moment(item.visitDate).diff(today.format('YYYY-MM-DD'), 'days') == 1,
                      ).length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">This week</td>
                  <td className="tableContents">
                    {
                      allScheduled.filter(
                        (item) =>
                          moment(item.visitDate).diff(today.format('YYYY-MM-DD'), 'days') > 1,
                      ).length
                    }
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <p className="dashboardTitle">Orders</p>
        <div className="tablesMainContainer">
          <div className="tableIndividualContainer">
            <p className="tableTitle">Orders by status</p>
            <table>
              <tbody>
                <tr>
                  <th className="tableHeaders">Status</th>
                  <th className="tableHeaders">Count</th>
                </tr>
                <tr>
                  <td className="tableContents">New Quotation</td>
                  <td className="tableContents">
                    {allOrders.filter((item) => item.status === 'live').length}
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">In Production</td>
                  <td className="tableContents">
                    {allOrders.filter((item) => item.status === 'inProduction').length}
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Out For Delivery</td>
                  <td className="tableContents">
                    {allOrders.filter((item) => item.status === 'outForDelivery').length}
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Delivered</td>
                  <td className="tableContents">
                    {allOrders.filter((item) => item.status === 'delivered').length}
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Installed</td>
                  <td className="tableContents">
                    {allOrders.filter((item) => item.status === 'installed').length}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="tableIndividualContainer">
            <p className="tableTitle">Quotation converted to order</p>
            <table>
              <tbody>
                <tr>
                  <th className="tableHeaders">Order</th>
                  <th className="tableHeaders">Count</th>
                </tr>
                <tr>
                  <td className="tableContents">Today</td>
                  <td className="tableContents">
                    {
                      allOrders.filter(
                        (item) =>
                          moment(item.addedDate).diff(today.format('YYYY-MM-DD'), 'days') == 0,
                      ).length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">This Week</td>
                  <td className="tableContents">
                    {
                      allOrders.filter((item) => today.diff(moment(item.addedDate), 'days') <= 6)
                        .length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">This Month</td>
                  <td className="tableContents">
                    {
                      allOrders.filter((item) => today.diff(moment(item.addedDate), 'months') == 0)
                        .length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">This Quarter</td>
                  <td className="tableContents">
                    {
                      allOrders.filter((item) => today.diff(moment(item.addedDate), 'months') <= 2)
                        .length
                    }
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="tableIndividualContainer">
            <p className="tableTitle">Scheduled Delivery Dates</p>
            <table>
              <tbody>
                <tr>
                  <th className="tableHeaders">Status</th>
                  <th className="tableHeaders">Count</th>
                </tr>
                <tr>
                  <td className="tableContents">Past Due</td>
                  <td className="tableContents">
                    {
                      allDeliveryDates.filter(
                        (item) =>
                          moment(item.estimatedDelivery).diff(today.format('YYYY-MM-DD'), 'days') <
                          0,
                      ).length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Today</td>
                  <td className="tableContents">
                    {
                      allDeliveryDates.filter(
                        (item) =>
                          moment(item.estimatedDelivery).diff(today.format('YYYY-MM-DD'), 'days') ==
                          0,
                      ).length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">Tomorrow</td>
                  <td className="tableContents">
                    {
                      allDeliveryDates.filter(
                        (item) =>
                          moment(item.estimatedDelivery).diff(today.format('YYYY-MM-DD'), 'days') ==
                          1,
                      ).length
                    }
                  </td>
                </tr>
                <tr>
                  <td className="tableContents">This week</td>
                  <td className="tableContents">
                    {
                      allDeliveryDates.filter(
                        (item) =>
                          moment(item.estimatedDelivery).diff(today.format('YYYY-MM-DD'), 'days') >
                          1,
                      ).length
                    }
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}
