import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDirections } from "@fortawesome/free-solid-svg-icons";
export default function ErrorPage() {
  return (
    <div className="coming-soon-page">
      <div className="coming-soon-box">
        <div className="vertical-space-120"></div>
        <div className="container">
          <div className="vertical-space-120"></div>
          <div className="coming-soon-details error">
            <div className="detail-box">
              <h2 className="df-color">404</h2>
              <h3>
                <span className="df-color">Page Not Found</span>
              </h3>
              <p>The page you might searching for is currently not available</p>
            </div>
          </div>
          <div className="vertical-space-120"></div>
        </div>
      </div>
    </div>
  );
}
