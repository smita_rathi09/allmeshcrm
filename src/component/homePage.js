import React, { useEffect } from 'react'
import '../styles/homePage.css'
import Dashboard from './dashboard'
import Header from './header'
import { useNavigate, Link } from 'react-router-dom'
import { Auth, API } from 'aws-amplify'
import { useSelector, useDispatch } from 'react-redux'
import * as queries from '../graphql/queries'
import axios from 'axios'
import { connect } from 'react-redux'

function HomePage(props) {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  useEffect(() => {
    async function fetchData() {
      await Auth.currentAuthenticatedUser({
        bypassCache: false,
      })
        .then((user) => {
          console.log('user ' + JSON.stringify(user))
        })
        .catch((err) => {
          console.log('err ' + JSON.stringify(err))
          if (err == 'The user is not authenticated') navigate('/')
        })
      try {
        const res = await axios.get(
          'https://allmeshimages.s3.ap-south-1.amazonaws.com/JSON/allMeshJSON.json',
          {
            headers: {
              'Cache-Control': 'no-cache',
            },
          },
        )
        dispatch({ type: 'SET_JSON', payload: res.data })
      } catch (error) {
        alert(JSON.stringify(error))
        console.log(JSON.stringify(error))
      }

      try {
        const allTodos = await API.graphql({
          query: queries.getUserDetails,
        })
        dispatch({ type: 'SET_USERDATA', payload: allTodos.data.getUserDetails })
      } catch (error) {
        console.log(JSON.stringify(error))
      }
    }
    fetchData()
  }, [])

  // console.log(orderStatusArray)
  return (
    <div>
      <div>
        <div>
          <p className="title">Welcome to AllMesh CRM portal</p>
        </div>
      </div>
      <Header id="homepage" />
      <div style={{ display: 'flex', justifyContent: 'right' }}>
        {props.common.sales.includes(props.common.role) && (
          <>
            <div className="viewAllQuotesContainer">
              <Link to="/ListOrder" className="viewAllQuotes">
                ORDERS
              </Link>
            </div>
            <div className="viewAllQuotesContainer">
              <Link to="/ListQuotations" className="viewAllQuotes">
                QUOTATIONS
              </Link>
            </div>
          </>
        )}
        {props.common.admin.includes(props.common.role) && (
          <>
            <div className="viewAllQuotesContainer">
              <Link to="/ListComponents" className="viewAllQuotes">
                COMPONENTS
              </Link>
            </div>
            <div className="viewAllQuotesContainer">
              <Link to="/ListGroups" className="viewAllQuotes">
                PRODUCT-GROUP
              </Link>
            </div>
          </>
        )}
        {props.common.manager.includes(props.common.role) && (
          <>
            <div className="viewAllQuotesContainer">
              <Link to="/ListBanners" className="viewAllQuotes">
                BANNERS
              </Link>
            </div>
            <div className="viewAllQuotesContainer">
              <Link to="/ListTestimonial" className="viewAllQuotes">
                TESTIMONIAL
              </Link>
            </div>
            <div className="viewAllQuotesContainer">
              <Link to="/ListGallery" className="viewAllQuotes">
                GALLERY
              </Link>
            </div>
            <div className="viewAllQuotesContainer">
              <Link to="/ListEMail" className="viewAllQuotes">
                E-MAIL LIST
              </Link>
            </div>
            <div className="viewAllQuotesContainer">
              <Link to="/Configuration" className="viewAllQuotes">
                CONFIGURATION
              </Link>
            </div>
          </>
        )}
        {props.common.owner.includes(props.common.role) && (
          <>
            <div className="viewAllQuotesContainer">
              <Link to="/listUsers" className="viewAllQuotes">
                MANAGE USERS
              </Link>
            </div>
          </>
        )}
      </div>
      <Dashboard />
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}

function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
