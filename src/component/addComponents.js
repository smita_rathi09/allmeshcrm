import { useNavigate, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import '../styles/addProducts.css'
import { Auth, API } from 'aws-amplify'
import * as mutations from '../graphql/mutations'
import * as queries from '../graphql/queries'
import moment from 'moment'
import uuidv4 from 'uuid/dist/v4'
import Header from './header'
import { connect } from 'react-redux'
import environment from '../configure'

function AddComponents(props) {
  const navigate = useNavigate()
  const [productName, setProductName] = useState('')
  const [unitOfMeasurement, setUnitOfMeasurement] = useState('')
  const [productDescription, setProductDescription] = useState('')
  const [productPrice, setProductPrice] = useState('')
  const [addedBy, setAddedBy] = useState('')
  const [updatedBy, setupdatedBy] = useState('')
  const [productImages, setProductImages] = useState([])
  const [productImagesURI, setProductImagesURI] = useState('')
  const [productImagesBase64, setProductImagesBase64] = useState('')
  const [spinnerState, setSpinnerState] = useState(false)
  const [calBasedOn, setCalBasedOn] = useState('')
  const [minValue, setMinValue] = useState('')
  const [unitPerPanel, setUnitPerPanel] = useState('')

  let test = useParams()

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        setAddedBy(user.username)
        setupdatedBy(user.username)
        if (test.test != 'addNewComponent') getItemById()
      })
      .catch((err) => {
        console.log('currentAuthenticatedUser ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.admin.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemById = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemById,
        variables: { id: test.test },
      })
      // console.log(allTodos.data.getItemById)
      let res = allTodos.data.getItemById
      if (res == null) navigate('/ListComponents')
      else {
        setProductName(res.productName)
        setUnitOfMeasurement(res.unitOfMeasurement)
        setProductDescription(res.productDescription)
        setProductPrice(res.productPrice)
        setProductImages(res.productImages)
        setProductImagesURI(res.productImages)
        setCalBasedOn(res.calBasedOn)
        setMinValue(res.minValue)
        setUnitPerPanel(res.unitPerPanel)
      }
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }
  const addItem = async (image) => {
    try {
      await API.graphql({
        query: mutations.addItem,
        variables: {
          productName: productName,
          unitOfMeasurement: unitOfMeasurement,
          productDescription: productDescription,
          productPrice: productPrice,
          addedBy: addedBy,
          productImages: image ?? productImages,
          type: 'Component',
          calBasedOn: calBasedOn,
          minValue: minValue,
          unitPerPanel: unitPerPanel,
        },
      })
      alert('Component Added')
      navigate('/ListComponents')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const editItem = async (image) => {
    try {
      await API.graphql({
        query: mutations.editItem,
        variables: {
          id: test.test,
          productName: productName,
          unitOfMeasurement: unitOfMeasurement,
          productDescription: productDescription,
          productPrice: productPrice,
          updatedBy: updatedBy,
          productImages: image ?? productImages,
          calBasedOn: calBasedOn,
          minValue: minValue,
          unitPerPanel: unitPerPanel,
        },
      })
      alert('Component updated')
      navigate('/ListComponents')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const checkInput = async () => {
    var numReg = /^[0-9]*$/
    if (productName === '') return alert('Please enter component name')
    if (unitOfMeasurement === '') return alert('Please select unit of measurement')
    if (productDescription === '') return alert('Please enter component description')
    if (productPrice === '') return alert('Please enter price')
    if (isNaN(productPrice)) return alert('Please enter valid price')
    if (calBasedOn === 'panel count' && unitPerPanel === '')
      return alert('Please enter unit per panel')
    if (numReg.test(unitPerPanel) === false) return alert('Please enter valid panel per unit')
    if (numReg.test(minValue) === false) return alert('Please enter valid min value')

    var tempImage = undefined
    if (
      (test.test === 'addNewComponent' && productImagesURI.length > 0) ||
      (test.test != 'addNewComponent' && productImagesURI != productImages)
    )
      try {
        const response = await fetch(environment.apiGateway + 'uploadImage', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            base64: productImagesBase64,
            fileName: uuidv4() + '.jpg',
          }),
        })
        const responseJson = await response.json()
        tempImage = responseJson.Location
      } catch (error) {
        console.error(error)
      }

    setSpinnerState('true')
    if (test.test === 'addNewComponent') addItem(tempImage)
    else editItem(tempImage)
  }

  const onImageChange = (event) => {
    let files = event.target.files
    let reader = new FileReader()
    reader.readAsDataURL(files[0])
    setProductImagesURI(URL.createObjectURL(files[0]))
    reader.onload = (e) => {
      setProductImagesBase64(e.target.result.replace(/^data:image\/\w+;base64,/, ''))
    }
  }

  return (
    <div>
      <Header />
      <div>
        <div>
          <p className="addProductTitle">Enter Component Details</p>
        </div>
        <div className="addProductFormContainer">
          <div className="fieldStyle">
            <span className="fieldName">Component Name:</span>
            <input
              className="addProdInputStyle"
              value={productName}
              onChange={(e) => setProductName(e.target.value)}
              maxLength={100}
            />
          </div>
          <div className="fieldStyle">
            <span className="fieldName">Unit Of Measurement:</span>
            <select
              className="addProdSelectStyle"
              value={unitOfMeasurement}
              onChange={(e) => setUnitOfMeasurement(e.target.value)}
            >
              <option value="">Select</option>;
              {props.common.UOMArray.map((item) => {
                return <option value={item}>{item}</option>
              })}
            </select>
          </div>
          <div className="fieldStyle">
            <span className="fieldName">Component Description:</span>
            <input
              className="addProdInputStyle"
              value={productDescription}
              onChange={(e) => setProductDescription(e.target.value)}
              maxLength={500}
            />
          </div>
          <div className="fieldStyle">
            <span className="fieldName" style={{ marginRight: 45 }}>
              Component Price
            </span>
            <input
              className="addProdInputStyle"
              value={productPrice}
              onChange={(e) => setProductPrice(e.target.value)}
              maxLength={10}
            />
          </div>
          <div className="fieldStyle">
            <span className="fieldName" style={{ marginRight: 45 }}>
              Calculation basis
            </span>
            <select
              className="addProdSelectStyle"
              value={calBasedOn}
              onChange={(e) => setCalBasedOn(e.target.value)}
            >
              <option value="">Select</option>;
              {props.common.calBasedOn.map((item) => {
                return <option value={item}>{item}</option>
              })}
            </select>
          </div>
          {calBasedOn === 'panel count' && (
            <div className="fieldStyle">
              <span className="fieldName" style={{ marginRight: 45 }}>
                Unit per panel
              </span>
              <input
                className="addProdInputStyle"
                value={unitPerPanel}
                onChange={(e) => setUnitPerPanel(e.target.value)}
                maxLength={2}
              />
            </div>
          )}

          <div className="fieldStyle">
            <span className="fieldName" style={{ marginRight: 45 }}>
              Minimum Value
            </span>
            <input
              className="addProdInputStyle"
              value={minValue}
              onChange={(e) => setMinValue(e.target.value)}
              maxLength={2}
            />
          </div>

          <div className="fieldStyle">
            <span className="fieldName checkBoxNames" style={{ marginRight: 45 }}>
              Component Image
            </span>
            <input type="file" name="myImage" onChange={onImageChange} accept="image/*" />
          </div>
          <img src={productImagesURI} width={150} style={{ marginLeft: '200px' }} />
          <p />
          <div className="fieldStyle">
            <button disabled={spinnerState} onClick={() => checkInput()} className="addProductBtn">
              {test.test === 'addNewComponent' ? `Add Component` : `Update Component`}
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}

function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(AddComponents)
