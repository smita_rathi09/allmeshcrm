import { useNavigate, useParams, Link } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import '../styles/addBanner.css'
import { Auth, API, label } from 'aws-amplify'
import * as mutations from '../graphql/mutations'
import * as queries from '../graphql/queries'
import Header from './header'
import { connect } from 'react-redux'
import DateTimePicker from 'react-datetime-picker'
import Modal from 'react-modal'
import { AiOutlineCloseCircle } from 'react-icons/ai'
import moment from 'moment'
import uuidv4 from 'uuid/dist/v4'
import { SpinnerCircular } from 'spinners-react'
import { pdf } from '@react-pdf/renderer'
import { saveAs } from 'file-saver'
import GeneratePDF from './generatePDF'
import { Alert } from '@mui/material'

function ViewQuotation(props) {
  const navigate = useNavigate()
  const [customerName, setCustomerName] = useState('')
  const [customerPhone, setCustomerPhone] = useState('')
  const [date, setDate] = useState('')
  const [estimatedAmount, setEstimatedAmount] = useState('')
  const [customerEmail, setCustomerEmail] = useState('')
  const [orderDetails, setOrderDetails] = useState({})
  const [status, setStatus] = useState('')
  const [addedBy, setAddedBy] = useState('')
  const [updatedBy, setupdatedBy] = useState('')
  const [spinnerState, setSpinnerState] = useState(false)
  const [scheduleDate, setScheduleDate] = useState(new Date())
  const [modal, setModal] = useState(false)
  const [modalAddGroups, setModalAddGroups] = useState(false)
  const [allProducts, setAllProducts] = useState([])
  const [width, setWidth] = useState('')
  const [quantity, setQuantity] = useState('')
  const [height, setHeight] = useState('')
  const [color, setColor] = useState('')
  const [notes, setNotes] = useState('')
  const [billingAddress, setBillingAddress] = useState('')
  const [shippingAddress, setShipingAddress] = useState('')
  const [visitDate, setVisitDate] = useState('')
  const [visitTime, setVisitTime] = useState('')
  const [nextStatus, setNextStatus] = useState('')
  const [rejectReason, setRejectReason] = useState('')
  const [customerReferenceNumber, setCustomerReferenceNumber] = useState('')
  const [panelCount, setPanelCount] = useState('')
  const [profileArray, setProfileArray] = useState([])
  const [modalCalculatePrice, setModalCalculatePrice] = useState(false)
  const [selectedGroup, setSelectedGroup] = useState([])
  const [allComponents, setAllComponents] = useState([])
  const [modalAddComponents, setModalAddComponents] = useState(false)
  const [productLocation, setProductLocation] = useState('')
  const [productType, setProductType] = useState('')
  const [estimatedDelivery, setEstimatedDelivery] = useState('')
  const [vendorList, setVendorList] = useState([])
  const [group, setGroup] = useState('')
  const [key, setkey] = useState('')
  const [quotationTrail, setQuotationTrail] = useState('')
  const [historyModal, setHistoryModal] = useState(false)
  const [addGroupFlag, setAddGroupFlag] = useState(false)
  const [editGroupFlag, setEditGroupFlag] = useState(false)
  const [oldDetails, setOldDetails] = useState([])
  const [term, setTerm] = useState('')
  const [time, setTime] = useState('')
  const [description, setDescription] = useState('')
  let { test, from } = useParams()

  useEffect(() => {
    var groups = []
    var components = []
    async function getCurrentUser() {
      try {
        const allTodos = await API.graphql({
          query: queries.getItemsByType,
          variables: { type: 'Group', status: 'live' },
        })
        // console.log(allTodos.data.getItemsByType)
        setAllProducts(allTodos.data.getItemsByType)
        groups = allTodos.data.getItemsByType
      } catch (error) {
        console.log(JSON.stringify(error))
      }

      try {
        const allTodos = await API.graphql({
          query: queries.getItemsByType,
          variables: { type: 'Configuration', status: 'live' },
        })
        // console.log(allTodos.data.getItemsByType)
      } catch (error) {
        console.log(JSON.stringify(error))
      }

      try {
        const allTodos = await API.graphql({
          query: queries.getItemsByType,
          variables: { type: 'Component', status: 'live' },
        })
        // console.log(allTodos.data.getItemsByType)
        setAllComponents(allTodos.data.getItemsByType)
        components = allTodos.data.getItemsByType
      } catch (error) {
        console.log(JSON.stringify(error))
      }

      try {
        await Auth.currentAuthenticatedUser({
          bypassCache: false,
        })
          .then((user) => {
            setAddedBy(user.username)
            setupdatedBy(user.username)
          })
          .catch((err) => {
            console.log('currentAuthenticatedUser ' + JSON.stringify(err))
          })
      } catch (e) {
        console.error(e)
      }
      if (test != 'addNewQuotation')
        try {
          const allTodos = await API.graphql({
            query: queries.getItemById,
            variables: { id: test },
          })
          let res = allTodos.data.getItemById
          setOldDetails(res)
          if (res == null) {
            if (from === 'quotation') navigate('/listQuotations')
            else navigate('/listOrder')
          } else {
            setCustomerName(res.customerName)
            setCustomerPhone(res.customerPhone.slice(3, 13))
            setDate(res.addedDate)
            setTime(res.time)
            setCustomerEmail(res.customerEmail)
            setBillingAddress(res.billingAddress)
            setShipingAddress(res.shippingAddress)
            setNotes(res.notes)
            if (res.visitDate) setVisitDate(res.visitDate)
            if (res.visitTime) setVisitTime(res.visitTime)
            setCustomerReferenceNumber(res.customerReferenceNumber)
            setQuotationTrail(res.quotationTrail ? JSON.parse(res.quotationTrail) : {})
            if (res.status != 'expired') {
              setOrderDetails(JSON.parse(res.orderDetails))
              setStatus(res.status)
              setEstimatedAmount(res.estimatedAmount)
              if (res.estimatedDelivery) setEstimatedDelivery(res.estimatedDelivery)
              props.setBillItems({
                orderDetails: JSON.parse(res.orderDetails),
                estimatedAmount: res.estimatedAmount,
                subTotal: res.subTotal ?? res.estimatedAmount,
                GST: res.GST ?? 0,
                GSTAmount: res.GSTAmount ?? 0,
                discountValue: res.discountValue ?? 0,
                discountType: res.discountType ?? 'flat',
                discountAmount: res.discountAmount ?? 0,
              })
            } else {
              await reNewQuotation(res, groups, components)
            }
          }
        } catch (error) {
          console.log(JSON.stringify(error))
        }
      else
        props.setBillItems({
          orderDetails: {},
          estimatedAmount: 0,
          subTotal: 0,
          GST: 0,
          GSTAmount: 0,
          discountValue: 0,
          discountType: 'flat',
          discountAmount: 0,
          componentList: {},
        })
    }
    getCurrentUser()
    if (!props.common.sales.includes(props.common.role)) navigate('/homePage')
  }, [test])

  const reNewQuotation = async (res, groups, components) => {
    let temp = JSON.parse(res.orderDetails)
    let total = 0

    Object.entries(temp).map(([key, value]) => {
      groups.map((group) => {
        if (value.id == group.id) {
          temp[key].productName = group.productName
          temp[key].productDescription = group.productDescription
          temp[key].productMargin = group.productMargin
          temp[key].itemTotal = 0
          temp[key].itemSubTotal = 0
          temp[key].marginAmount = 0

          let tempComponentList = temp[key].componentList
          temp[key].componentList = JSON.parse(group.componentList)
          components.map((comp) => {
            if (Object.keys(temp[key].componentList).includes(comp.id)) {
              temp[key].componentList[comp.id].productName = comp.productName
              temp[key].componentList[comp.id].productDescription = comp.productDescription
              temp[key].componentList[comp.id].productImages = comp.productImages
              temp[key].componentList[comp.id].unitOfMeasurement = comp.unitOfMeasurement
              temp[key].componentList[comp.id].productPrice = comp.productPrice
              if (comp.calBasedOn) temp[key].componentList[comp.id].calBasedOn = comp.calBasedOn
              if (comp.minValue) temp[key].componentList[comp.id].minValue = comp.minValue
              if (comp.unitPerPanel)
                temp[key].componentList[comp.id].unitPerPanel = comp.unitPerPanel
              temp[key].componentList[comp.id].componentTotal =
                parseFloat(comp.productPrice) * tempComponentList[comp.id].count
              temp[key].itemSubTotal =
                parseFloat(temp[key].itemSubTotal) +
                parseFloat(comp.productPrice) * tempComponentList[comp.id].count
              temp[key].componentList[comp.id].count = tempComponentList[comp.id].count
            }
          })

          temp[key].itemSubTotal =
            parseFloat(temp[key].itemSubTotal) * parseFloat(temp[key].quantity)

          var colorCharges =
            parseFloat(temp[key].quantity) *
            parseFloat(props.common.color[temp[key].color].pricePerFeet) *
            parseFloat(temp[key].areaInSqFt)

          temp[key].itemSubTotal = parseFloat(temp[key].itemSubTotal) + parseFloat(colorCharges)

          temp[key].marginAmount =
            (parseFloat(group.productMargin) / 100) * parseFloat(temp[key].itemSubTotal)
          temp[key].itemTotal =
            parseFloat(temp[key].itemSubTotal) + parseFloat(temp[key].marginAmount)
          temp[key].itemPrice = parseFloat(temp[key].itemTotal) / parseFloat(temp[key].quantity)

          total = parseFloat(total) + parseFloat(temp[key].itemTotal)
        }
      })
    })
    setOrderDetails(temp)
    setStatus('live')
    setDate(moment(new Date()).format('YYYY-MM-DD'))
    setTime(new Date().toTimeString().substr(0, 8))
    setEstimatedAmount(total)
    if (res.estimatedDelivery) setEstimatedDelivery(res.estimatedDelivery)
    props.setBillItems({
      orderDetails: temp,
      estimatedAmount: parseFloat(total).toFixed(2),
      subTotal: parseFloat(total).toFixed(2),
      GST: 0,
      GSTAmount: 0,
      discountValue: 0,
      discountType: 'flat',
      discountAmount: 0,
    })
  }

  const editItem = async () => {
    let tempHistory = []
    let tempTrail = quotationTrail

    if (oldDetails.customerName !== customerName)
      tempHistory.push({
        name: 'Customer Name',
        oldValue: oldDetails.customerName,
        newValue: customerName,
      })

    if (oldDetails.customerPhone.slice(3, 13) !== customerPhone)
      tempHistory.push({
        name: 'Customer Phone',
        oldValue: oldDetails.customerPhone,
        newValue: '+91' + customerPhone,
      })

    if (oldDetails.customerEmail !== customerEmail)
      tempHistory.push({
        name: 'Customer Email',
        oldValue: oldDetails.customerEmail,
        newValue: customerEmail,
      })

    if (oldDetails.notes !== notes)
      tempHistory.push({
        name: 'Notes',
        oldValue: oldDetails.notes,
        newValue: notes,
      })
    if (oldDetails.billingAddress !== billingAddress)
      tempHistory.push({
        name: 'Billing Address',
        oldValue: oldDetails.billingAddress,
        newValue: billingAddress,
      })
    if (oldDetails.shippingAddress !== shippingAddress)
      tempHistory.push({
        name: 'Shipping Address',
        oldValue: oldDetails.shippingAddress,
        newValue: shippingAddress,
      })
    if (oldDetails.status !== status)
      tempHistory.push({
        name: 'Status',
        oldValue: oldDetails.status,
        newValue: status,
      })
    if ((oldDetails.discountType ?? 0) != props.bill.discountType)
      tempHistory.push({
        name: 'Discount Type',
        oldValue: oldDetails.discountType ?? 0,
        newValue: props.bill.discountType,
      })
    if ((oldDetails.discountValue ?? 0) != props.bill.discountValue)
      tempHistory.push({
        name: 'Discount Value',
        oldValue: oldDetails.discountValue ?? 0,
        newValue: props.bill.discountValue,
      })
    if ((oldDetails.discountAmount ?? 0) != props.bill.discountAmount)
      tempHistory.push({
        name: 'Discount Amount',
        oldValue: '₹' + oldDetails.discountAmount ?? 0,
        newValue: '₹' + props.bill.discountAmount,
      })
    if ((oldDetails.GST ?? 0) != props.bill.GST)
      tempHistory.push({
        name: 'GST',
        oldValue: oldDetails.GST ?? 0 + '%',
        newValue: props.bill.GST + '%',
      })
    if ((oldDetails.estimatedAmount ?? 0) != props.bill.total)
      tempHistory.push({
        name: 'Total',
        oldValue: '₹' + oldDetails.estimatedAmount ?? 0,
        newValue: '₹' + props.bill.total,
      })

    var oldItems = JSON.parse(oldDetails.orderDetails)
    var items = { ...props.bill.items }
    Object.entries(oldItems).map(([key1, value1]) => {
      if (Object.keys(items).includes(key1)) {
        if (value1.itemTotal != items[key1].itemTotal) {
          tempHistory.push({
            name: 'Modified: ' + value1.productName,
            oldValue: value1.itemTotal,
            newValue: items[key1].itemTotal,
          })
          delete items[key1]
        } else delete items[key1]
      } else
        tempHistory.push({
          name: 'Deleted: ' + value1.productName,
          oldValue: value1.itemTotal,
          newValue: '',
        })
    })

    if (Object.entries(items).length > 0) {
      Object.entries(items).map(([key2, value2]) => {
        tempHistory.push({
          name: 'Added: ' + value2.productName,
          oldValue: '',
          newValue: value2.itemTotal,
        })
      })
    }

    // return alert(JSON.stringify(tempHistory))

    if (tempHistory.length > 0) {
      const id = uuidv4()
      tempTrail[id] = {}
      tempTrail[id].id = id
      tempTrail[id].updatedBy = updatedBy
      tempTrail[id].date = moment(new Date()).format('YYYY-MM-DD hh:mm:ss A')
      tempTrail[id].history = tempHistory
    }

    try {
      await API.graphql({
        query: mutations.editItem,
        variables: {
          id: test,
          customerName: customerName,
          customerPhone: '+91' + customerPhone,
          customerEmail: customerEmail,
          billingAddress: billingAddress,
          shippingAddress: shippingAddress,
          notes: notes,
          orderDetails: JSON.stringify(props.bill.items),
          updatedBy: updatedBy,
          estimatedAmount: props.bill.total,
          subTotal: props.bill.subTotal,
          GST: props.bill.GST,
          GSTAmount: props.bill.GSTAmount,
          discountValue: props.bill.discountValue,
          discountType: props.bill.discountType,
          discountAmount: props.bill.discountAmount,
          quotationTrail: JSON.stringify(tempTrail),
          addedDate: date,
          status: status,
          time: time,
        },
      })
      if (from === 'quotation') {
        alert('Quotation updated')
        navigate('/listQuotations')
      } else {
        alert('Order updated')
        navigate('/listOrder')
      }
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const addItem = async () => {
    var RandomNumber = Math.floor(Math.random() * 10000000000) + 1

    try {
      await API.graphql({
        query: mutations.addItem,
        variables: {
          type: 'Quotation',
          addedBy: addedBy,
          customerName: customerName,
          customerPhone: '+91' + customerPhone,
          customerEmail: customerEmail,
          billingAddress: billingAddress,
          shippingAddress: shippingAddress,
          notes: notes,
          orderDetails: JSON.stringify(props.bill.items),
          estimatedAmount: props.bill.total,
          customerReferenceNumber: RandomNumber,
          time: new Date().toTimeString().substr(0, 8),
          subTotal: props.bill.subTotal,
          GST: props.bill.GST,
          GSTAmount: props.bill.GSTAmount,
          discountValue: props.bill.discountValue,
          discountType: props.bill.discountType,
          discountAmount: props.bill.discountAmount,
        },
      })
      alert('Quotation Added')
      navigate('/listQuotations')
    } catch (error) {
      console.log(JSON.stringify(error))
      // alert(JSON.stringify(error))
    }
  }

  const checkInput = async () => {
    if (customerName === '') return alert('Please enter customer name')
    if (customerPhone === '') return alert('Please enter customer phone')
    var phoneReg = /^[5-9]{1}[0-9]{9}$/
    if (phoneReg.test(customerPhone) === false) return alert('Please enter valid phone number')
    var mailReg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    if (customerEmail !== '' && mailReg.test(customerEmail) === false)
      return alert('Please enter valid email')
    if (Object.entries(props.bill.items).length == 0) return alert('Order list cannot be empty')
    if (props.bill.GST === '') return alert('Please select GST')
    if (props.bill.discountValue === '') return alert('Please enter discount')
    if (
      props.bill.discountValue != '' &&
      props.bill.discountValue > 0 &&
      props.bill.discountType === ''
    )
      return alert('Please select type of discount')
    if (isNaN(props.bill.total)) return
    setSpinnerState(true)
    if (test === 'addNewQuotation') addItem()
    else editItem()
  }

  const updateStatus = async (nextStatus) => {
    try {
      await API.graphql({
        query: mutations.updateStatus,
        variables: {
          id: test,
          visitDate: scheduleDate.toISOString().substr(0, 10),
          visitTime: scheduleDate.toTimeString().substr(0, 8),
          status: nextStatus,
          rejectReason: rejectReason,
          updatedBy: updatedBy,
          customerName: customerName,
          customerPhone: '+91' + customerPhone,
          customerEmail: customerEmail,
          billingAddress: billingAddress,
          shippingAddress: shippingAddress,
          notes: notes,
          estimatedAmount: props.bill.total,
          orderDetails: JSON.stringify(props.bill.items),
          customerReferenceNumber: customerReferenceNumber,
          subTotal: props.bill.subTotal,
          GST: props.bill.GST,
          GSTAmount: props.bill.GSTAmount,
          discountValue: props.bill.discountValue,
          discountType: props.bill.discountType,
          discountAmount: props.bill.discountAmount,
          time: new Date().toTimeString().substr(0, 8),
        },
      })
      alert('Status updated')
      setModal(false)
      navigate('/listQuotations')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }
  const updateOrderStatus = async (nextStatus) => {
    if (nextStatus == 'inProduction' || nextStatus == 'outForDelivery') {
      if (group == '') return alert('Please select group')
      if (vendorList.length == 0) return alert('No mail id is present in this group')
    }
    try {
      await API.graphql({
        query: mutations.updateOrderStatus,
        variables: {
          id: test,
          status: nextStatus,
          updatedBy: updatedBy,
          estimatedDelivery: scheduleDate.toISOString().substr(0, 10),
          vendorList: vendorList,
          rejectReason: rejectReason,
        },
      })
      alert('Status updated')
      setModal(false)
      navigate('/listOrder')
    } catch (error) {
      alert(JSON.stringify(error))
    }
  }

  const takeInputs = (nextStatus) => {
    var msgText = 'Are you sure you want to '
    var statusText = ''
    if (nextStatus === 'visitScheduled') statusText = 'schedule a visit?'
    if (nextStatus === 'convertedToOrder') statusText = 'convert quotation to order?'
    if (nextStatus === 'rejected') statusText = 'reject this quotation/order?'
    if (nextStatus === 'deleted') statusText = 'delete this quotation/order?'
    if (nextStatus === 'inProduction') statusText = 'start production for this order?'
    if (nextStatus === 'outForDelivery') statusText = 'mark this order for delivery?'
    if (nextStatus === 'delivered') statusText = 'mark this order as delivered?'
    if (nextStatus === 'installed') statusText = 'mark this order as installed?'

    if (window.confirm(msgText + statusText)) {
      if (from === 'quotation') {
        if (nextStatus === 'visitScheduled' || nextStatus === 'rejected') setModal(true)
        else {
          updateStatus(nextStatus)
        }
      } else if (from === 'order') {
        if (
          nextStatus == 'inProduction' ||
          nextStatus == 'outForDelivery' ||
          nextStatus === 'rejected'
        )
          setModal(true)
        else {
          updateOrderStatus(nextStatus)
        }
      }
    }
  }

  const getVendorList = async (group) => {
    setSpinnerState(true)
    var tempVendorList = []
    try {
      const allTodos = await API.graphql({
        query: mutations.manageEmail,
        variables: { type: 'Email', action: 'listEmail' },
      })
      var res = JSON.parse(allTodos.data.manageEmail.output)
      res.map((item) => {
        if (item.group === group && item.verificationStatus == 'Success')
          tempVendorList.push(item.email)
      })
      setVendorList(tempVendorList)
      setSpinnerState(false)
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const combinationUtil = (arr, data, start, end, index, r) => {
    {
      let temp = []
      if (index == r) {
        for (let j = 0; j < r; j++) temp.push(data[j])
        profileArray.push(temp)
      }
      for (let i = start; i <= end && end - i + 1 >= r - index; i++) {
        data[index] = arr[i]
        combinationUtil(arr, data, i + 1, end, index + 1, r)
      }
    }
  }
  const printCombination = (arr, n, r) => {
    {
      let data = new Array(r)
      combinationUtil(arr, data, 0, n - 1, 0, r)
    }
  }

  const getComponentDetails = async (item) => {
    if (productLocation == '') return alert('Please select product location')
    if (productType == '') return alert('Please select product type')

    let temp = JSON.parse(item.componentList)
    allComponents.map((item) => {
      if (Object.keys(temp).includes(item.id)) {
        temp[item.id].productName = item.productName
        temp[item.id].productDescription = item.productDescription
        temp[item.id].productImages = item.productImages
        temp[item.id].unitOfMeasurement = item.unitOfMeasurement
        temp[item.id].productPrice = item.productPrice
        temp[item.id].componentTotal = 0
        if (item.calBasedOn) temp[item.id].calBasedOn = item.calBasedOn
        if (item.minValue) temp[item.id].minValue = item.minValue
        if (item.unitPerPanel) temp[item.id].unitPerPanel = item.unitPerPanel
      }
    })

    setAddGroupFlag(true)
    props.setComponentsEdit({
      componentList: temp,
      productMargin: item.productMargin,
      areaInSqFt: 0,
      groupSubTotal: 0,
      groupPrice: 0,
      marginAmount: 0,
      groupTotal: 0,
      colorCharges: 0,
    })
    setSelectedGroup(item)
    setDescription(item.productDescription)
    setModalCalculatePrice(true)
  }

  const editProductGroup = (key, item) => {
    setWidth(item.width)
    setHeight(item.height)
    setColor(item.color)
    setProductLocation(item.productLocation)
    setProductType(item.productType)
    setPanelCount(item.panelCount)
    setQuantity(item.quantity)
    setEditGroupFlag(true)
    props.setComponentsEdit({
      componentList: item.componentList,
      groupSubTotal: item.itemSubTotal,
      groupPrice: item.itemPrice,
      productMargin: item.productMargin,
      marginAmount: item.marginAmount,
      groupTotal: item.itemTotal,
      areaInSqFt: item.areaInSqFt,
      colorCharges: parseFloat(item.colorCharges),
    })

    setSelectedGroup(item)
    setDescription(item.productDescription)
    setkey(key)
    setModalCalculatePrice(true)
  }

  const addGroupInput = async () => {
    var numReg = /^[0-9]*$/
    if (productType == '') return alert('Please select product type')
    if (productLocation == '') return alert('Please select product location')
    if (panelCount == '') return alert('Please enter number of door/panel')
    if (numReg.test(panelCount) === false) return alert('Please enter valid count')
    if (width == '') return alert('Please enter width in mm')
    if (numReg.test(width) === false) return alert('Please enter valid width')
    if (height == '') return alert('Please enter height in mm')
    if (color == '') return alert('Please select color')
    if (numReg.test(height) === false) return alert('Please enter valid height')
    if (Object.entries(props.bill.componentList).length == 0)
      return alert('Component list cannot be empty')

    let flag = false
    Object.entries(props.bill.componentList).map(([key, value]) => {
      if (value.count === '') {
        alert('Enter count for ' + value.productName)
        return (flag = true)
      }
    })

    if (flag) return

    props.addToBill({
      item: selectedGroup,
      width: width,
      height: height,
      color: color,
      selectedColorCharge: props.common.color[color].pricePerFeet,
      areaInSqFt: props.bill.areaInSqFt,
      subTotal: props.bill.groupSubTotal,
      productMargin: props.bill.productMargin,
      marginAmount: props.bill.marginAmount,
      total: props.bill.groupTotal,
      price: props.bill.groupPrice,
      panelCount: panelCount,
      componentList: props.bill.componentList,
      productLocation: productLocation,
      productType: productType,
      key: key,
      quantity: quantity,
      description: description,
      colorCharges: props.bill.colorCharges,
    })

    setModalCalculatePrice(false)
    setModalAddGroups(false)
    setAddGroupFlag(false)
    setEditGroupFlag(false)
    setProductType('')
    setProductLocation('')
    setWidth('')
    setColor('')
    setHeight('')
    setPanelCount('')
    setQuantity('')
  }

  // useEffect(() => {}, [spinnerState])
  useEffect(() => {
    if (panelCount !== '' && width !== '' && height !== '' && color !== '') {
      if (addGroupFlag && panelCount < 4) calculateProfile()
      if (editGroupFlag) console.log('Do nothing')
    } else if (panelCount == '' && width == '' && height == '' && color == '')
      console.log('Do nothing as everything is 0')
    else {
      console.log('Calculation needs to be changed')
      var tempObj = props.bill.componentList
      Object.entries(tempObj).map(([key, value]) => {
        if (value.count) {
          value.componentTotal = 0
          value.count = ''
        }
      })
      props.setComponentsEdit({
        componentList: tempObj,
        groupSubTotal: 0,
        groupPrice: 0,
        productMargin: props.bill.productMargin,
        marginAmount: 0,
        groupTotal: 0,
        areaInSqFt: parseFloat(width * 0.00328084 * height * 0.00328084).toFixed(2),
        colorCharges: 0,
      })
    }

    if (width !== '' && height !== '') props.setAreaSqFt({ width: width, height: height })
  }, [width, height, panelCount, color])

  const calculateProfile = async () => {
    if (panelCount !== '' && width !== '' && height !== '' && color !== '') {
      setSpinnerState(true)
      let sideArray = []
      let widthArray = []
      let heightArray = []
      let profile12 = 0
      let profile7 = 0
      let widthSide = Math.ceil((width / panelCount) * 0.00328084)
      let heightSide = Math.ceil(height * 0.00328084)
      while (widthSide > 12) {
        widthSide = widthSide - 12
        profile12 = profile12 + panelCount * 2
      }
      while (heightSide > 12) {
        heightSide = heightSide - 12
        profile12 = profile12 + panelCount * 2
      }
      widthArray = Array(panelCount * 2).fill(widthSide)
      heightArray = Array(panelCount * 2).fill(heightSide)
      sideArray = widthArray.concat(heightArray)
      setProfileArray([])
      for (let i = 1; i <= sideArray.length; i++)
        await printCombination(sideArray, sideArray.length, i)
      profileArray.map((value) => {
        var sum = value.reduce(function (a, b) {
          return a + b
        }, 0)
        if (sum == 12 || sum == 7) {
          let temp = [...sideArray]
          let findElements = true
          value.map((item) => {
            if (temp.includes(item)) {
              const index1 = temp.indexOf(item)
              if (index1 > -1) temp.splice(index1, 1)
              return
            } else findElements = false
          })

          if (findElements) {
            value.map((item) => {
              const index1 = sideArray.indexOf(item)
              if (index1 > -1) sideArray.splice(index1, 1)
            })

            if (sum == 12) profile12++
            else if (sum == 7) profile7++
          }
        }
      })
      for (let total = 11; total > 0; total--) {
        if (total == 7) continue
        if (sideArray.length > 0) {
          profileArray.map((value) => {
            var sum = value.reduce(function (a, b) {
              return a + b
            }, 0)
            let temp = [...sideArray]
            let findElements = true
            value.map((item) => {
              if (temp.includes(item)) {
                const index1 = temp.indexOf(item)
                if (index1 > -1) temp.splice(index1, 1)
                return
              } else findElements = false
            })

            if (findElements && sum == total) {
              value.map((item) => {
                const index1 = sideArray.indexOf(item)
                if (index1 > -1) sideArray.splice(index1, 1)
              })

              if (total > 7) {
                profile12++
              } else if (total < 7) {
                profile7++
              }
            }
          })
        }
      }
      setSpinnerState(false)

      var areaInSqFt = width * 0.00328084 * height * 0.00328084

      let temp = props.bill.componentList
      Object.entries(temp).map(([key, value]) => {
        if (value.calBasedOn) {
          if (value.calBasedOn == '7 feet') temp[key].count = profile7
          else if (value.calBasedOn == '12 feet') temp[key].count = profile12
          else if (value.calBasedOn === 'panel count')
            value.count = parseFloat(value.unitPerPanel) * parseFloat(panelCount)
          else if (value.calBasedOn === 'sq feet') value.count = parseFloat(areaInSqFt).toFixed(2)
        }
      })

      props.setComponents({
        componentList: temp,
        groupSubTotal: 0,
        groupPrice: 0,
        productMargin: props.bill.productMargin,
        marginAmount: 0,
        groupTotal: 0,
        areaInSqFt: parseFloat(areaInSqFt).toFixed(2),
        quantity: quantity,
        selectedColorCharge: color != '' ? props.common.color[color].pricePerFeet : 0,
      })
    }
  }

  const customStyles = {
    content: {
      top: '50%',
      width: '500px',
      height: '400px',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  }

  const customStylesAddProducts = {
    content: {
      top: '50%',
      width: '1100px',
      height: '600px',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  }

  const downloadPDF = async () => {
    await pdf(<GeneratePDF item={oldDetails} />)
      .toBlob()
      .then((blob) => saveAs(blob, `${customerReferenceNumber}.pdf`))
  }

  return (
    <div>
      <Header />
      {/* <SpinnerCircular enabled={spinnerState} /> */}
      <div>
        <p className="addBannerTitle">
          {from === 'quotation' ? 'Quotation' : 'Order'} Details: {customerReferenceNumber}
        </p>

        <div style={{ display: 'flex', justifyContent: 'left', margin: '5px' }}>
          <div style={{ width: '400px', marginLeft: '20px' }}>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Customer Name:</span>
              <input
                className="addProdInputStyle"
                value={customerName}
                onChange={(e) => setCustomerName(e.target.value)}
                maxLength={100}
              />
            </div>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Customer Phone:</span>
              <div style={{ direction: 'flex' }}>
                <span className="addProdInputStyle">+91</span>
                <input
                  className="addProdInputStyle"
                  value={customerPhone}
                  onChange={(e) => setCustomerPhone(e.target.value)}
                  maxLength={10}
                />
              </div>
            </div>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Customer Email:</span>
              <input
                className="addProdInputStyle"
                value={customerEmail}
                onChange={(e) => setCustomerEmail(e.target.value)}
                maxLength={100}
              />
            </div>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Notes:</span>
              <input
                className="addProdInputStyle"
                value={notes}
                onChange={(e) => setNotes(e.target.value)}
                maxLength={500}
              />
            </div>
          </div>
          <div style={{ width: '400px', marginLeft: '20px' }}>
            {from === 'quotation' && status === 'visitScheduled' && (
              <div className="addBannerFieldStyle">
                <span className="addBannerFieldName">
                  Customer visit is on {moment(visitDate, 'YYYY-MM-DD').format('D MMM')} at{' '}
                  {moment(visitTime, 'HH:mm:ss').format('hh:mm A')}
                </span>
              </div>
            )}
            {from === 'order' && status != 'delivered' && estimatedDelivery != '' && (
              <div className="addBannerFieldStyle">
                <span className="addBannerFieldName">
                  Estimated delivery date:
                  {moment(estimatedDelivery, 'YYYY-MM-DD').format('D MMM')}
                </span>
              </div>
            )}
            {from === 'quotation' && test != 'addNewQuotation' && (
              <div className="addBannerFieldStyle">
                <span className="addBannerFieldName">
                  Enquiry Date: {moment(date, 'YYYY-MM-DD').format('D MMM')}
                </span>
              </div>
            )}
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Billing Address:</span>
              <input
                className="addProdInputStyle"
                value={billingAddress}
                onChange={(e) => setBillingAddress(e.target.value)}
                maxLength={500}
              />
            </div>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Shipping Address:</span>
              <input
                className="addProdInputStyle"
                value={shippingAddress}
                onChange={(e) => setShipingAddress(e.target.value)}
                maxLength={500}
              />
            </div>
            <div className="addBannerFieldStyle">
              <span style={{ color: 'dodgerblue' }} onClick={() => setHistoryModal(true)}>
                Quotation History
              </span>
              <span
                style={{ color: 'dodgerblue' }}
                onClick={() => setShipingAddress(billingAddress)}
              >
                Copy billing address
              </span>
            </div>
          </div>

          <div style={{ width: '400px', marginLeft: '20px' }}>
            {test != 'addNewQuotation' && (
              <div className="addBannerFieldStyle">
                <p></p>
                <button className="addProductBtn" onClick={() => downloadPDF()}>
                  Download PDF
                </button>
              </div>
            )}
            {status !== 'convertedToOrder' && (
              <>
                <div className="addBannerFieldStyle">
                  <p></p>
                  <button
                    disabled={spinnerState}
                    onClick={() => checkInput()}
                    className="addProductBtn"
                  >
                    {from === 'quotation'
                      ? test === 'addNewQuotation'
                        ? `Add Quotation`
                        : `Update Quotation`
                      : 'Save Order'}
                  </button>
                </div>
                {status !== 'outForDelivery' && (
                  <div className="addBannerFieldStyle">
                    <p></p>
                    <button
                      disabled={spinnerState}
                      onClick={() => {
                        setProductType('')
                        setProductLocation('')
                        setWidth('')
                        setHeight('')
                        setColor('')
                        setPanelCount('')
                        setQuantity(1)
                        setModalAddGroups(true)
                      }}
                      className="addProductBtn"
                    >
                      Add Product Group
                    </button>
                  </div>
                )}
                {test != 'addNewQuotation' && (
                  <div className="addBannerFieldStyle">
                    <p></p>
                    <select
                      value={nextStatus}
                      onChange={(e) => {
                        setNextStatus(e.target.value)
                        takeInputs(e.target.value)
                      }}
                      className="addProductBtn"
                    >
                      <option value="">Change Status</option>;
                      {(from === 'quotation' && (
                        <>
                          <option value="visitScheduled">Schedule Visit</option>;
                          <option value="convertedToOrder">Convert to Order</option>;
                          <option value="rejected">Reject</option>
                          <option value="deleted">Delete</option>;
                        </>
                      )) || (
                        <>
                          <option value="inProduction">In-Production</option>;
                          <option value="outForDelivery">Out for Delivery</option>;
                          <option value="delivered">Delivered</option>;
                          <option value="installed">Installed</option>;
                          <option value="rejected">Reject</option>;
                          <option value="deleted">Delete</option>;
                        </>
                      )}
                    </select>
                  </div>
                )}
              </>
            )}
          </div>
        </div>
        <div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-around',
              alignItems: 'center',
              borderBottom: '1px solid gray',
            }}
          >
            <p style={{ fontWeight: 'bold', width: 200 }}>Product Name</p>
            <p style={{ fontWeight: 'bold', width: 50 }}>Width (mm)</p>
            <p style={{ fontWeight: 'bold', width: 50 }}>Height (mm)</p>
            <p style={{ fontWeight: 'bold', width: 50 }}>Color</p>
            <p style={{ fontWeight: 'bold', width: 50 }}>Panel Count</p>
            <p style={{ fontWeight: 'bold', width: 80 }}>Location</p>
            <p style={{ fontWeight: 'bold', width: 80 }}>Type</p>
            <p style={{ fontWeight: 'bold', width: 80 }}>Sub-Total (₹) </p>
            <p style={{ fontWeight: 'bold', width: 80 }}>Quantity</p>
            <p style={{ fontWeight: 'bold', width: 80 }}>Total (₹) </p>
            {status !== 'convertedToOrder' && status !== 'delivered' && (
              <>
                <p style={{ fontWeight: 'bold', width: 60 }}>Edit</p>
                <p style={{ fontWeight: 'bold', width: 60 }}>Remove</p>
              </>
            )}
          </div>

          {Object.entries(props.bill.items).map(([key, value]) => {
            return (
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  borderBottom: '1px solid gray',
                }}
              >
                <p style={{ width: 200 }}>
                  {value.productName}({value.productDescription})
                </p>
                <p style={{ width: 50 }}>{value.width}</p>
                <p style={{ width: 50 }}>{value.height}</p>
                <p style={{ width: 50 }}>{value.color}</p>
                <p style={{ width: 50 }}>{value.panelCount}</p>
                <p style={{ width: 80 }}>{value.productLocation}</p>
                <p style={{ width: 80 }}>{value.productType}</p>
                <p style={{ width: 80 }}>₹ {parseFloat(value.itemPrice).toFixed(2)}</p>
                <p style={{ width: 80 }}>
                  {value.quantity > 1 && (
                    <button
                      style={{ width: 20 }}
                      onClick={() =>
                        props.decrementItem({
                          key: key,
                          selectedColorCharge: props.common.color[value.color].pricePerFeet,
                        })
                      }
                    >
                      -
                    </button>
                  )}{' '}
                  {value.quantity}{' '}
                  <button
                    style={{ width: 20 }}
                    onClick={() =>
                      props.incrementItem({
                        key: key,
                        selectedColorCharge: props.common.color[value.color].pricePerFeet,
                      })
                    }
                  >
                    +
                  </button>
                </p>
                <p style={{ width: 80 }}>₹ {parseFloat(value.itemTotal).toFixed(2)}</p>
                {status !== 'convertedToOrder' && status !== 'delivered' && (
                  <>
                    <button style={{ width: 60 }} onClick={() => editProductGroup(key, value)}>
                      Edit
                    </button>
                    <button style={{ width: 60 }} onClick={() => props.removeFromBill(key)}>
                      Remove
                    </button>
                  </>
                )}
              </div>
            )
          })}
        </div>
        <div style={{ display: 'flex', justifyContent: 'right', margin: '5px' }}>
          <div style={{ width: '400px', marginLeft: '20px', alignItems: 'right' }}>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Sub-Total:</span>
              <span className="addProdInputStyle">
                {parseFloat(props.bill.subTotal).toFixed(2)}
              </span>
            </div>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Discount:</span>
              <div style={{ display: 'flex' }}>
                <input
                  style={{ width: '90px' }}
                  value={props.bill.discountValue}
                  onChange={(e) =>
                    props.setExtraCharges({
                      GST: props.bill.GST,
                      discountValue: e.target.value,
                      discountType: props.bill.discountType,
                      discountAmount: props.bill.discountAmount,
                    })
                  }
                  maxLength={10}
                />
                <select
                  style={{ width: '90px' }}
                  value={props.bill.discountType}
                  onChange={(e) => {
                    props.setExtraCharges({
                      GST: props.bill.GST,
                      discountValue: props.bill.discountValue,
                      discountType: e.target.value,
                      discountAmount: props.bill.discountAmount,
                    })
                  }}
                >
                  <option value="flat">Flat</option>;<option value="percentage">Percentage</option>;
                </select>
              </div>
            </div>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Discount Amount:</span>
              <span className="addProdInputStyle">
                {parseFloat(props.bill.discountAmount).toFixed(2)}
              </span>
            </div>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">GST:</span>
              <div style={{ direction: 'flex' }}>
                <span className="addBannerFieldName" style={{ width: '90px' }}>
                  {parseFloat(props.bill.GSTAmount).toFixed(2)}
                </span>
                <select
                  className="addProdInputStyle"
                  style={{ width: '90px' }}
                  value={props.bill.GST}
                  onChange={(e) => {
                    props.setExtraCharges({
                      GST: e.target.value,
                      discountValue: props.bill.discountValue,
                      discountType: props.bill.discountType,
                      discountAmount: props.bill.discountAmount,
                    })
                  }}
                >
                  <option value="">Select</option>
                  <option value="18">18%</option>;<option value="12">12%</option>;
                </select>
              </div>
            </div>
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Total Amount: </span>
              <span className="addProdInputStyle">
                {isNaN(props.bill.total) ? '' : parseFloat(props.bill.total).toFixed(2)}
              </span>
            </div>
          </div>
        </div>
      </div>
      <Modal isOpen={modal} style={customStyles}>
        <div>
          <div className="closeButtonContainer">
            <button
              className="closeButton"
              onClick={() => {
                setModal(false)
                setNextStatus('')
              }}
            >
              <AiOutlineCloseCircle />
            </button>
          </div>
          {from === 'quotation' && nextStatus === 'visitScheduled' && (
            <>
              <p>Select schedule date and time</p>
              <div>
                <DateTimePicker
                  onChange={setScheduleDate}
                  value={scheduleDate}
                  minDate={new Date()}
                />
              </div>
            </>
          )}
          {(from === 'quotation' || from === 'order') && nextStatus === 'rejected' && (
            <div className="addBannerFieldStyle">
              <span className="addBannerFieldName">Reason For rejection:</span>
              <input
                className="addProdInputStyle"
                value={rejectReason}
                onChange={(e) => setRejectReason(e.target.value)}
                maxLength={200}
              />
            </div>
          )}

          {from === 'order' && nextStatus != 'rejected' && (
            <div>
              <p>Estimated Delivery Date</p>
              <div>
                <DateTimePicker
                  onChange={setScheduleDate}
                  value={scheduleDate}
                  minDate={new Date()}
                />
              </div>

              <div className="addBannerFieldStyle">
                <span className="addBannerFieldName">Group: </span>
                <select
                  value={group}
                  onChange={(e) => {
                    setGroup(e.target.value)
                    getVendorList(e.target.value)
                  }}
                >
                  <option value="">Select Group</option>;
                  {props.common.groupArray.map((item) => {
                    return <option value={item}>{item}</option>
                  })}
                </select>
              </div>
              <SpinnerCircular enabled={spinnerState} />
              {vendorList.length > 0 && (
                <div className="addBannerFieldStyle">
                  <p>Order details E-mail will got to below {vendorList.length} id(s)</p>
                </div>
              )}
              <div className="addBannerFieldStyle">
                <p style={{ width: 70 }}>
                  {vendorList.map((item) => {
                    return (
                      <span>
                        {item}
                        {', '}
                      </span>
                    )
                  })}
                </p>
              </div>
            </div>
          )}
          <div className="closeButtonContainer">
            <button
              className="addProductBtn"
              enabled={!spinnerState}
              onClick={() => {
                from === 'quotation' ? updateStatus(nextStatus) : updateOrderStatus(nextStatus)
              }}
            >
              Ok
            </button>
          </div>
        </div>
      </Modal>
      <Modal isOpen={modalAddGroups} style={customStylesAddProducts}>
        <div>
          <div className="closeButtonContainer">
            <button className="closeButton" onClick={() => setModalAddGroups(false)}>
              <AiOutlineCloseCircle />
            </button>
          </div>
          <div className="addProductFormContainer">
            <div className="fieldStyle">
              <span style={{ fontWeight: 'bold' }}>Select Product Group</span>
            </div>
            <div className="fieldStyle">
              <span>Select product location:</span>
              <select
                className="addProdInputStyle"
                value={productLocation}
                onChange={(e) => {
                  setProductLocation(e.target.value)
                  setProductType('')
                }}
              >
                <option value="">Select</option>;
                {Object.entries(props.common.productLocationTypeMap).map(([key, value]) => {
                  return <option value={key}>{key}</option>
                })}
              </select>
            </div>
            {productLocation !== '' && (
              <div className="fieldStyle">
                <span>Select product type:</span>
                <select
                  className="addProdInputStyle"
                  value={productType}
                  onChange={(e) => setProductType(e.target.value)}
                >
                  <option value="">Select</option>;
                  {Object.entries(props.common.productLocationTypeMap[productLocation].value).map(
                    ([key, value]) => {
                      return <option value={key}>{key}</option>
                    },
                  )}
                </select>
              </div>
            )}
          </div>

          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p style={{ fontWeight: 'bold', width: 250 }}>Product-Group Name</p>
            <p style={{ fontWeight: 'bold', width: 200 }}>Description</p>
            <p style={{ fontWeight: 'bold', width: 70 }}></p>
          </div>
          {allProducts
            .filter(
              (item) =>
                item.productType.includes(productType) &&
                item.productLocation.includes(productLocation),
            )
            .map((item) => {
              return (
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    borderBottom: '1px solid gray',
                  }}
                >
                  <p style={{ width: 250 }}>{item.productName}</p>
                  <p style={{ width: 200 }}>{item.productDescription}</p>

                  <div style={{ width: 70 }}>
                    <button className="addProductBtn" onClick={() => getComponentDetails(item)}>
                      Add
                    </button>
                  </div>
                </div>
              )
            })}
        </div>
      </Modal>
      <Modal isOpen={modalCalculatePrice} style={customStylesAddProducts}>
        <div>
          <div className="closeButtonContainer">
            <button className="closeButton" onClick={() => setModalCalculatePrice(false)}>
              <AiOutlineCloseCircle />
            </button>
          </div>
          {/* <SpinnerCircular enabled={spinnerState} /> */}
          <div>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div>
                <div className="fieldStyle">
                  <span style={{ fontWeight: 'bold' }}>Product Group Details:</span>
                </div>
                <div className="fieldStyle">
                  <span>Name: {selectedGroup.productName}</span>
                </div>

                <div className="fieldStyle">
                  <span>
                    Description:
                    {/* {selectedGroup.productDescription} */}
                    <input
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                      maxLength={200}
                    />
                  </span>
                </div>
                <div className="fieldStyle">
                  <span>Quantity: {quantity}</span>
                </div>
                <div className="fieldStyle">
                  <span>
                    Color Charges:
                    {parseFloat(props.bill.colorCharges).toFixed(2)}
                  </span>
                  {/* <input
                    value={props.bill.colorCharges}
                    onChange={(e) => {
                      if (e.target.value == '.') {
                        props.colorChargesChanged({
                          colorCharges: e.target.value,
                          quantity: quantity,
                        })
                      } else if (isNaN(e.target.value)) alert('Please enter valid value')
                      else
                        props.colorChargesChanged({
                          colorCharges: e.target.value,
                          quantity: quantity,
                        })
                    }}
                  /> */}
                </div>
                <div className="fieldStyle">
                  <span>Sub-Total: {parseFloat(props.bill.groupSubTotal).toFixed(2)}</span>
                </div>
                <div className="fieldStyle">
                  <span>
                    Extra Charges: {parseFloat(props.bill.marginAmount).toFixed(2)} (
                    {props.bill.productMargin}%)
                  </span>
                </div>
                <div className="fieldStyle">
                  <span>Total: {parseFloat(props.bill.groupTotal).toFixed(2)}</span>
                </div>
              </div>
              <div>
                <div className="fieldStyle">
                  <span>Product location: {productLocation}</span>
                </div>
                <div className="fieldStyle">
                  <span>Product type: {productType}</span>
                </div>
                <div className="fieldStyle">
                  <span>Enter number of doors/panel </span>
                  <input
                    value={panelCount}
                    onChange={(e) => setPanelCount(e.target.value)}
                    maxLength={2}
                  />
                </div>

                <div className="fieldStyle">
                  <span>Width(in mm) </span>
                  <input value={width} onChange={(e) => setWidth(e.target.value)} maxLength={10} />
                </div>
                <div className="fieldStyle">
                  <span>Height(in mm) </span>
                  <input
                    value={height}
                    onChange={(e) => setHeight(e.target.value)}
                    maxLength={10}
                  />
                </div>
                <div className="fieldStyle">
                  <span>Color </span>
                  <select
                    className="addProdInputStyle"
                    value={color}
                    onChange={(e) => {
                      setColor(e.target.value)
                      props.colorChanged({
                        selectedColorCharge: props.common.color[e.target.value].pricePerFeet ?? 0,
                        quantity: quantity,
                      })
                    }}
                  >
                    <option value="">Select</option>;
                    {Object.entries(props.common.color).map(([key, value]) => {
                      return <option value={key}>{key}</option>
                    })}
                  </select>
                </div>
                <div className="fieldStyle">
                  <span>Area(in sqft): {parseFloat(props.bill.areaInSqFt).toFixed(2)}</span>
                </div>
                <div className="fieldStyle">
                  <span>Price per group: {parseFloat(props.bill.groupPrice).toFixed(2)}</span>
                </div>
              </div>
            </div>
          </div>

          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p style={{ fontWeight: 'bold', width: 250 }}>Product Name</p>
            <p style={{ fontWeight: 'bold', width: 200 }}>Description</p>
            <p style={{ fontWeight: 'bold', width: 70 }}>Price</p>
            <p style={{ fontWeight: 'bold', width: 70 }}>Count</p>
            <p style={{ fontWeight: 'bold', width: 70 }}>Component Total</p>
            <p style={{ fontWeight: 'bold', width: 70 }}></p>
          </div>

          {props.bill.componentList &&
            Object.entries(props.bill.componentList)
              // .filter((item) => item.productType.includes(productType))
              .map(([key, value]) => {
                return (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      borderBottom: '1px solid gray',
                    }}
                  >
                    <p style={{ width: 250 }}>{value.productName}</p>
                    <p style={{ width: 200 }}>{value.productDescription}</p>
                    <p style={{ width: 70 }}>{parseFloat(value.productPrice).toFixed(2)}</p>
                    <div>
                      <input
                        style={{ width: 70 }}
                        value={value.count}
                        onChange={(e) => {
                          if (e.target.value == '.') {
                            props.changeComponentCount({
                              count: e.target.value,
                              id: value.id,
                              quantity: quantity,
                            })
                          } else if (isNaN(e.target.value)) alert('Please enter valid value')
                          else
                            props.changeComponentCount({
                              count: e.target.value,
                              id: value.id,
                              quantity: quantity,
                            })
                        }}
                        maxLength={10}
                      />
                    </div>
                    <p style={{ width: 70 }}>{parseFloat(value.componentTotal).toFixed(2)}</p>

                    <div style={{ width: 70 }}>
                      <button
                        className="addProductBtn"
                        onClick={() => props.removeComponent({ id: value.id, quantity: quantity })}
                      >
                        Remove
                      </button>
                    </div>
                  </div>
                )
              })}
          {/* <div className="fieldStyle">
            <span />
            <span style={{ paddingRight: 175 }}>
              Total: {parseFloat(props.bill.groupSubTotal / quantity).toFixed(2)}
            </span>
          </div> */}
          <div className="fieldStyle">
            <button className="addProductBtn" onClick={() => setModalAddComponents(true)}>
              Add Components
            </button>
            <button
              className="addProductBtn"
              onClick={() => {
                addGroupInput()
              }}
            >
              OK
            </button>
          </div>
        </div>
      </Modal>
      <Modal isOpen={modalAddComponents} style={customStylesAddProducts}>
        <div>
          <div className="closeButtonContainer">
            <button className="closeButton" onClick={() => setModalAddComponents(false)}>
              <AiOutlineCloseCircle />
            </button>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            <span style={{ fontWeight: 'bold' }}>Select Components</span>
            <input
              className="searchInputBox"
              value={term}
              onChange={(e) => setTerm(e.target.value)}
              placeholder="search by component name"
            />
          </div>

          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p style={{ fontWeight: 'bold', width: 250 }}>Product Name</p>
            <p style={{ fontWeight: 'bold', width: 200 }}>Description</p>
            <p style={{ fontWeight: 'bold', width: 70 }}>Price</p>
            <p style={{ fontWeight: 'bold', width: 70 }}></p>
          </div>

          {(term == ''
            ? Object.entries(allComponents)
            : Object.entries(allComponents).filter(
                ([key, value]) =>
                  value.productName.toString().toLowerCase().indexOf(term.toLowerCase()) > -1,
              )
          ).map(([key, value]) => {
            return (
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  borderBottom: '1px solid gray',
                }}
              >
                <p style={{ width: 250 }}>{value.productName}</p>
                <p style={{ width: 200 }}>{value.productDescription}</p>
                <p style={{ width: 70 }}>{parseFloat(value.productPrice).toFixed(2)}</p>
                {props.bill.componentList && (
                  <div style={{ width: 70 }}>
                    <button
                      className="addProductBtn"
                      onClick={() =>
                        Object.keys(props.bill.componentList).includes(value.id)
                          ? props.removeComponent({ id: value.id, quantity: quantity })
                          : props.addComponent({ value: value, quantity: quantity })
                      }
                    >
                      {Object.keys(props.bill.componentList).includes(value.id) ? 'Remove' : 'Add'}
                    </button>
                  </div>
                )}
              </div>
            )
          })}
          <div className="fieldStyle">
            <button className="addProductBtn" onClick={() => setModalAddComponents(false)}>
              Ok
            </button>
          </div>
        </div>
      </Modal>
      <Modal isOpen={historyModal} style={customStylesAddProducts}>
        <div>
          <div className="closeButtonContainer">
            <button className="closeButton" onClick={() => setHistoryModal(false)}>
              <AiOutlineCloseCircle />
            </button>
          </div>
          <div className="fieldStyle">
            <span style={{ fontWeight: 'bold' }}>Quotation History</span>
          </div>
          {Object.entries(quotationTrail)
            .sort(
              (a, b) =>
                moment(b[1].date, 'YYYY-MM-DD hh:mm:ss A') -
                moment(a[1].date, 'YYYY-MM-DD hh:mm:ss A'),
            )
            .map(([key, value]) => {
              return (
                <>
                  <div>
                    <p style={{ fontWeight: 'bold' }}>
                      {value.updatedBy} changed on{' '}
                      {moment(value.date, 'YYYY-MM-DD hh:mm:ss A').format('D MMM hh:mm')}
                    </p>
                    {value.history.map((item) => (
                      <p>
                        {item.name} : {item.oldValue}
                        {item.oldValue != '' && item.newValue != '' ? '-->' : ''} {item.newValue}
                      </p>
                    ))}
                  </div>
                </>
              )
            })}

          <div className="fieldStyle">
            <button className="addProductBtn" onClick={() => setHistoryModal(false)}>
              Ok
            </button>
          </div>
        </div>
      </Modal>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
    bill: state.bill,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    addToBill: (data) => dispatch({ type: 'ADD_ITEM', payload: data }),
    removeFromBill: (data) => dispatch({ type: 'REMOVE_ITEM', payload: data }),
    incrementItem: (data) => dispatch({ type: 'INCREMENT_ITEM', payload: data }),
    decrementItem: (data) => dispatch({ type: 'DECREMENT_ITEM', payload: data }),
    setBillItems: (data) => dispatch({ type: 'SET_BILL_ITEMS', payload: data }),
    setComponents: (data) => dispatch({ type: 'SET_COMPONENTS', payload: data }),
    setComponentsEdit: (data) => dispatch({ type: 'SET_COMPONENTS_EDIT', payload: data }),
    addComponent: (data) => dispatch({ type: 'ADD_COMPONENT', payload: data }),
    removeComponent: (data) => dispatch({ type: 'REMOVE_COMPONENT', payload: data }),
    changeComponentCount: (data) => dispatch({ type: 'CHANGE_COM_COUNT', payload: data }),
    setExtraCharges: (data) => dispatch({ type: 'SET_EXTRA_CHARGES', payload: data }),
    setAreaSqFt: (data) => dispatch({ type: 'SET_AEAR_SQFT', payload: data }),
    colorChanged: (data) => dispatch({ type: 'COLOR_CHANGED', payload: data }),
    colorChargesChanged: (data) => dispatch({ type: 'COLOR_CHARGES_CHANGED', payload: data }),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ViewQuotation)
