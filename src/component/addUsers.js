import { useNavigate, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import '../styles/addBanner.css'
import { Auth, API } from 'aws-amplify'
import * as mutations from '../graphql/mutations'
import * as queries from '../graphql/queries'
import Header from './header'
import { connect } from 'react-redux'

function AddUser(props) {
  const navigate = useNavigate()
  const [username, setUserName] = useState('')
  const [role, setRole] = useState('')
  const [updatedBy, setupdatedBy] = useState('')
  const [spinnerState, setSpinnerState] = useState(false)

  let test = useParams()

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        setupdatedBy(user.username)
        getItemById()
      })
      .catch((err) => {
        console.log('currentAuthenticatedUser ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.owner.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemById = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemById,
        variables: { id: test.test },
      })
      let res = allTodos.data.getItemById
      if (res == null) navigate('/listUsers')
      else {
        setUserName(res.username)
        setRole(res.role)
      }
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const editItem = async (image) => {
    try {
      await API.graphql({
        query: mutations.editItem,
        variables: { id: test.test, role: role, updatedBy: updatedBy },
      })
      alert('User updated')
      navigate('/listUsers')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const checkInput = async () => {
    editItem()
  }

  return (
    <div>
      <Header />
      <div>
        <div>
          <p className="addBannerTitle">User Details</p>
        </div>
        <div style={{ width: '400px', marginLeft: '20px' }}>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">User Name:</span>
            <span className="addProdInputStyle">{username}</span>
          </div>
          <div className="addBannerFieldStyle">
            <span className="addBannerFieldName">Role: </span>
            <select
              className="addProdInputStyle"
              value={role}
              onChange={(e) => setRole(e.target.value)}
            >
              {props.common.roleArray.map((value) => {
                return <option value={value}>{value}</option>
              })}
            </select>
          </div>

          <p />
          <div className="addBannerFieldStyle">
            <button disabled={spinnerState} onClick={() => checkInput()} className="addProductBtn">
              Edit User
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}

function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(AddUser)
