import { useNavigate } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import '../styles/addProducts.css'
import { Auth, API } from 'aws-amplify'
import * as mutations from '../graphql/mutations'
import * as queries from '../graphql/queries'
import Header from './header'
import { connect } from 'react-redux'

function Configuration(props) {
  const navigate = useNavigate()
  const [id, setId] = useState('')
  const [aluminiumRate, setAluminiumRate] = useState('')
  const [updatedBy, setupdatedBy] = useState('')
  const [spinnerState, setSpinnerState] = useState(false)

  useEffect(async () => {
    await Auth.currentAuthenticatedUser({
      bypassCache: false,
    })
      .then((user) => {
        setupdatedBy(user.username)
        getItemsByTypeMap()
      })
      .catch((err) => {
        console.log('currentAuthenticatedUser ' + JSON.stringify(err))
        if (err == 'The user is not authenticated') navigate('/')
      })
    if (!props.common.manager.includes(props.common.role)) navigate('/homePage')
  }, [])

  const getItemsByTypeMap = async () => {
    try {
      const allTodos = await API.graphql({
        query: queries.getItemsByType,
        variables: { type: 'Configuration', status: 'live' },
      })
      // console.log(allTodos.data.getItemsByType)
      setId(allTodos.data.getItemsByType[0].id)
      setAluminiumRate(allTodos.data.getItemsByType[0].aluminiumRate)
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const editItem = async () => {
    try {
      await API.graphql({
        query: mutations.editItem,
        variables: {
          id: id,
          aluminiumRate: aluminiumRate,
          updatedBy: updatedBy,
        },
      })
      alert('Configuration updated')
      navigate('/HomePage')
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

  const checkInput = async () => {
    if (aluminiumRate === '') return alert('Please enter price')
    if (isNaN(aluminiumRate)) return alert('Please enter valid price')
    setSpinnerState('true')
    editItem()
  }

  return (
    <div>
      <Header />
      <div>
        <div>
          <p className="addProductTitle">Enter Configuration Details</p>
        </div>
        <div className="addProductFormContainer">
          <div className="fieldStyle">
            <span className="fieldName">Aluminium Rate:</span>
            <input
              className="addProdInputStyle"
              value={aluminiumRate}
              onChange={(e) => setAluminiumRate(e.target.value)}
              maxLength={4}
            />
          </div>
          <p />
          <div className="fieldStyle">
            <button disabled={spinnerState} onClick={() => checkInput()} className="addProductBtn">
              Update Configuration
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    common: state.common,
  }
}
function mapDispatchToProps(dispatch) {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(Configuration)
