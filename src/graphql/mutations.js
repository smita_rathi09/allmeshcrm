/* eslint-disable */
// this is an auto generated file. This will be overwritten
import gql from 'graphql-tag'

export const createUserDetails = gql`
  mutation createUserDetails(
    $username: String
    $preferred_username: String
    $name: String
    $role: String
  ) {
    createUserDetails(
      input: {
        username: $username
        preferred_username: $preferred_username
        name: $name
        role: $role
      }
    ) {
      id
    }
  }
`

export const addItem = gql`
  mutation addItem(
    $priceLog: AWSJSON
    $productType: String
    $addedBy: String
    $productMinHeight: String
    $frameColor: [String]
    $productMaterial: String
    $productMinWidth: String
    $unitOfMeasurement: String
    $productDescription: String
    $productImages: [String]
    $productColor: [String]
    $productPrice: String
    $productMaxHeight: String
    $productMaxWidth: String
    $productName: String
    $text1: String
    $text2: String
    $text3: String
    $type: String
    $componentList: AWSJSON
    $productSubType: String
    $productLocation: String
    $customerName: String
    $customerPhone: String
    $customerEmail: String
    $billingAddress: String
    $shippingAddress: String
    $notes: String
    $orderDetails: AWSJSON
    $estimatedAmount: Float
    $customerReferenceNumber: String
    $time: String
    $subTotal: Float
    $GST: Float
    $GSTAmount: Float
    $discountValue: Float
    $discountType: String
    $discountAmount: Float
    $calBasedOn: String
    $minValue: String
    $unitPerPanel: String
    $productMargin: String
  ) {
    addItem(
      input: {
        priceLog: $priceLog
        productType: $productType
        addedBy: $addedBy
        productMinHeight: $productMinHeight
        frameColor: $frameColor
        productMaterial: $productMaterial
        productMinWidth: $productMinWidth
        unitOfMeasurement: $unitOfMeasurement
        productDescription: $productDescription
        productImages: $productImages
        productColor: $productColor
        productPrice: $productPrice
        productMaxHeight: $productMaxHeight
        productMaxWidth: $productMaxWidth
        productName: $productName
        text1: $text1
        text2: $text2
        text3: $text3
        type: $type
        componentList: $componentList
        productSubType: $productSubType
        productLocation: $productLocation
        customerName: $customerName
        customerPhone: $customerPhone
        customerEmail: $customerEmail
        billingAddress: $billingAddress
        shippingAddress: $shippingAddress
        notes: $notes
        orderDetails: $orderDetails
        estimatedAmount: $estimatedAmount
        customerReferenceNumber: $customerReferenceNumber
        time: $time
        subTotal: $subTotal
        GST: $GST
        GSTAmount: $GSTAmount
        discountValue: $discountValue
        discountType: $discountType
        discountAmount: $discountAmount
        calBasedOn: $calBasedOn
        minValue: $minValue
        unitPerPanel: $unitPerPanel
        productMargin: $productMargin
      }
    ) {
      id
    }
  }
`

export const editItem = gql`
  mutation editItem(
    $id: String
    $priceLog: AWSJSON
    $productType: String
    $addedBy: String
    $productMinHeight: String
    $frameColor: [String]
    $productMaterial: String
    $productMinWidth: String
    $unitOfMeasurement: String
    $productDescription: String
    $productImages: [String]
    $productColor: [String]
    $productPrice: String
    $productMaxHeight: String
    $productMaxWidth: String
    $productName: String
    $updatedBy: String
    $text1: String
    $text2: String
    $text3: String
    $customerName: String
    $customerPhone: String
    $customerEmail: String
    $billingAddress: String
    $shippingAddress: String
    $notes: String
    $orderDetails: AWSJSON
    $estimatedAmount: Float
    $name: String
    $phoneNumber: String
    $group: String
    $role: String
    $componentList: AWSJSON
    $aluminiumRate: String
    $productSubType: String
    $productLocation: String
    $subTotal: Float
    $GST: Float
    $GSTAmount: Float
    $discountValue: Float
    $discountType: String
    $discountAmount: Float
    $quotationTrail: AWSJSON
    $addedDate: String
    $status: String
    $calBasedOn: String
    $minValue: String
    $unitPerPanel: String
    $productMargin: String
    $time: String
  ) {
    editItem(
      input: {
        id: $id
        priceLog: $priceLog
        productType: $productType
        addedBy: $addedBy
        productMinHeight: $productMinHeight
        frameColor: $frameColor
        productMaterial: $productMaterial
        productMinWidth: $productMinWidth
        unitOfMeasurement: $unitOfMeasurement
        productDescription: $productDescription
        productImages: $productImages
        productColor: $productColor
        productPrice: $productPrice
        productMaxHeight: $productMaxHeight
        productMaxWidth: $productMaxWidth
        productName: $productName
        updatedBy: $updatedBy
        text1: $text1
        text2: $text2
        text3: $text3
        customerName: $customerName
        customerPhone: $customerPhone
        customerEmail: $customerEmail
        billingAddress: $billingAddress
        shippingAddress: $shippingAddress
        notes: $notes
        orderDetails: $orderDetails
        estimatedAmount: $estimatedAmount
        name: $name
        phoneNumber: $phoneNumber
        group: $group
        role: $role
        componentList: $componentList
        aluminiumRate: $aluminiumRate
        productSubType: $productSubType
        productLocation: $productLocation
        subTotal: $subTotal
        GST: $GST
        GSTAmount: $GSTAmount
        discountValue: $discountValue
        discountType: $discountType
        discountAmount: $discountAmount
        quotationTrail: $quotationTrail
        addedDate: $addedDate
        status: $status
        calBasedOn: $calBasedOn
        minValue: $minValue
        unitPerPanel: $unitPerPanel
        productMargin: $productMargin
        time: $time
      }
    ) {
      id
    }
  }
`

export const deleteItem = gql`
  mutation deleteItem($id: String) {
    deleteItem(id: $id) {
      id
    }
  }
`

export const updateStatus = gql`
  mutation updateStatus(
    $id: String
    $updatedBy: String
    $visitDate: String
    $visitTime: String
    $status: String
    $rejectReason: String
    $customerName: String
    $customerPhone: String
    $customerEmail: String
    $billingAddress: String
    $shippingAddress: String
    $notes: String
    $orderDetails: AWSJSON
    $customerReferenceNumber: String
    $estimatedAmount: Float
    $subTotal: Float
    $GST: Float
    $GSTAmount: Float
    $discountValue: Float
    $discountType: String
    $discountAmount: Float
    $time: String
  ) {
    updateStatus(
      input: {
        id: $id
        updatedBy: $updatedBy
        visitDate: $visitDate
        visitTime: $visitTime
        status: $status
        rejectReason: $rejectReason
        customerName: $customerName
        customerPhone: $customerPhone
        customerEmail: $customerEmail
        billingAddress: $billingAddress
        shippingAddress: $shippingAddress
        notes: $notes
        orderDetails: $orderDetails
        customerReferenceNumber: $customerReferenceNumber
        estimatedAmount: $estimatedAmount
        subTotal: $subTotal
        GST: $GST
        GSTAmount: $GSTAmount
        discountValue: $discountValue
        discountType: $discountType
        discountAmount: $discountAmount
        time: $time
      }
    ) {
      id
    }
  }
`

export const updateOrderStatus = gql`
  mutation updateOrderStatus(
    $id: String
    $updatedBy: String
    $status: String
    $estimatedDelivery: String
    $vendorList: [String]
    $rejectReason: String
  ) {
    updateOrderStatus(
      input: {
        id: $id
        updatedBy: $updatedBy
        status: $status
        estimatedDelivery: $estimatedDelivery
        vendorList: $vendorList
        rejectReason: $rejectReason
      }
    ) {
      id
    }
  }
`

export const manageEmail = gql`
  mutation manageEmail(
    $addedBy: String
    $email: String
    $action: String
    $type: String
    $name: String
    $phoneNumber: String
    $group: String
    $role: String
    $notes: String
    $id: String
  ) {
    manageEmail(
      input: {
        addedBy: $addedBy
        email: $email
        action: $action
        type: $type
        name: $name
        phoneNumber: $phoneNumber
        group: $group
        role: $role
        notes: $notes
        id: $id
      }
    ) {
      id
      output
      name
      phoneNumber
      group
      role
      notes
    }
  }
`
