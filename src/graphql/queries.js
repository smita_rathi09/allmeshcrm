/* eslint-disable */
// this is an auto generated file. This will be overwritten
import gql from 'graphql-tag'

export const getUserDetails = gql`
  query getUserDetails {
    getUserDetails {
      id
      username
      preferred_username
      name
      role
    }
  }
`

export const getItemsByType = gql`
  query getItemsByType($type: String, $status: String) {
    getItemsByType(type: $type, status: $status) {
      id
      priceLog
      productType
      addedBy
      productMinHeight
      status
      frameColor
      productMaterial
      productMinWidth
      unitOfMeasurement
      productDescription
      productImages
      addedDate
      productColor
      productPrice
      productMaxHeight
      productMaxWidth
      productName
      customerPhone
      customerName
      # date
      estimatedAmount
      time
      customerEmail
      orderDetails
      type
      text1
      text2
      text3
      billingAddress
      shippingAddress
      notes
      visitDate
      visitTime
      customerReferenceNumber
      componentList
      aluminiumRate
      productSubType
      productLocation
      calBasedOn
      minValue
      unitPerPanel
      productMargin
      username
      role
    }
  }
`

export const getItemById = gql`
  query getItemById($id: String) {
    getItemById(id: $id) {
      id
      priceLog
      productType
      addedBy
      productMinHeight
      status
      frameColor
      productMaterial
      productMinWidth
      unitOfMeasurement
      productDescription
      productImages
      addedDate
      productColor
      productPrice
      productMaxHeight
      productMaxWidth
      productName
      text1
      text2
      text3
      customerPhone
      customerName
      # date
      estimatedAmount
      time
      customerEmail
      orderDetails
      billingAddress
      shippingAddress
      notes
      visitDate
      visitTime
      customerReferenceNumber
      estimatedDelivery
      email
      name
      phoneNumber
      group
      role
      componentList
      productSubType
      productLocation
      subTotal
      GST
      GSTAmount
      discountValue
      discountType
      discountAmount
      quotationTrail
      calBasedOn
      minValue
      unitPerPanel
      productMargin
      username
    }
  }
`

export const getAllItemsByType = gql`
  query getAllItemsByType($type: String) {
    getAllItemsByType(type: $type) {
      id
      priceLog
      productType
      addedBy
      productMinHeight
      status
      frameColor
      productMaterial
      productMinWidth
      unitOfMeasurement
      productDescription
      productImages
      addedDate
      productColor
      productPrice
      productMaxHeight
      productMaxWidth
      productName
      customerPhone
      customerName
      # date
      estimatedAmount
      time
      customerEmail
      orderDetails
      type
      text1
      text2
      text3
      billingAddress
      shippingAddress
      notes
      visitDate
      visitTime
      customerReferenceNumber
      estimatedDelivery
      componentList
      productSubType
      productLocation
      calBasedOn
      minValue
      unitPerPanel
      productMargin
    }
  }
`
