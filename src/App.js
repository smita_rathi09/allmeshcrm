import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './index.css'
import HomePage from './component/homePage.js'
import SignIn from './component/signIn.js'
import SignUp from './component/signUp.js'
import ListQuotations from './component/listQuotations'
import AddBanner from './component/addBanner'
import ListBanners from './component/listBanners'
import AddTestimonial from './component/addTestimonial'
import ListTestimonial from './component/listTestimonial'
import ViewQuotation from './component/viewQuotation'
import Dashboard from './component/dashboard'
import ListOrder from './component/listOrder'
import ListEMail from './component/listEMail'
import AddEMail from './component/addEMail'
import AddComponents from './component/addComponents'
import ListComponents from './component/listComponents'
import AddGroups from './component/addGroups'
import ListGroups from './component/listGroups'
import Configuration from './component/configuration'
import GeneratePDF from './component/generatePDF'
import AddUsers from './component/addUsers'
import ListUsers from './component/listUsers'
import AddGallery from './component/addGallery'
import ListGallery from './component/listGallery'

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" index element={<SignIn />} />
          <Route path="SignUp" element={<SignUp />} />
          <Route path="HomePage" element={<HomePage />} />
          <Route path="ListQuotations" element={<ListQuotations />} />
          <Route path="AddBanner/:test" element={<AddBanner />} />
          <Route path="ListBanners" element={<ListBanners />} />
          <Route path="AddTestimonial/:test" element={<AddTestimonial />} />
          <Route path="ListTestimonial" element={<ListTestimonial />} />
          <Route path="ViewQuotation/:test/:from" element={<ViewQuotation />} />
          <Route path="Dashboard" element={<Dashboard />} />
          <Route path="ListOrder" element={<ListOrder />} />
          <Route path="ListEMail" element={<ListEMail />} />
          <Route path="AddEMail/:test" element={<AddEMail />} />
          <Route path="AddComponents/:test" element={<AddComponents />} />
          <Route path="ListComponents" element={<ListComponents />} />
          <Route path="AddGroups/:test" element={<AddGroups />} />
          <Route path="ListGroups" element={<ListGroups />} />
          <Route path="Configuration" element={<Configuration />} />
          <Route path="GeneratePDF/:test" element={<GeneratePDF />} />
          <Route path="AddUsers/:test" element={<AddUsers />} />
          <Route path="ListUsers" element={<ListUsers />} />
          <Route path="AddGallery/:test" element={<AddGallery />} />
          <Route path="ListGallery" element={<ListGallery />} />
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default App
